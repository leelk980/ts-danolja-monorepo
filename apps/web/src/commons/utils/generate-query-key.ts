import { ServerApi } from '@danolja/shared';
import { buildUri } from '@danolja/web/commons/utils/build-uri';

export const generateQueryKey = <T extends ServerApi & { method: 'get' }>(
  api: T,
  options: { params?: unknown[]; queryString?: T['queryString'] } = {},
): [string, T['queryString']] => {
  const { params, queryString } = options;

  return [buildUri(api.uri, params), queryString];
};
