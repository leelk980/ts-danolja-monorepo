export function buildUri(uri: string, params?: unknown[]) {
  if (!params || params.filter((v) => !v).length > 0) {
    return uri;
  }

  return uri
    .split('/')
    .map((each) => (each.startsWith(':') ? params.shift() : each))
    .join('/');
}
