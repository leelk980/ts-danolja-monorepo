export enum LocalStorageEnum {
  ACCESS_TOKEN = 'accessToken',
  REFRESH_TOKEN = 'refreshToken',
}

export enum SessionStorageEnum {}

export enum CookieStorageEnum {}
