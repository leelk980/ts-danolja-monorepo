import { useToast, VStack } from '@chakra-ui/react';
import { ApiRouter, SocketEvent } from '@danolja/shared';
import { ChatSpace } from '@danolja/web/components/quiz-rooms-id/ChatSpace';
import { Header } from '@danolja/web/components/quiz-rooms-id/Header';
import { Navigation, NavigationTab } from '@danolja/web/components/quiz-rooms-id/Navigation';
import { Quiz, QuizSetting } from '@danolja/web/components/quiz-rooms-id/QuizSetting';
import { QuizSpace } from '@danolja/web/components/quiz-rooms-id/QuizSpace';
import { ScoreBoard } from '@danolja/web/components/quiz-rooms-id/ScoreBoard';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { socketService } from '@danolja/web/services/socket.service';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const QuizRoomsIdPage: NextPage = () => {
  const router = useRouter();
  const [id, setId] = useState(0);
  const [currNavTab, setCurrNavTab] = useState<NavigationTab>('quizSpace');
  const [quizzes, setQuizzes] = useState<Quiz[]>([]);
  const { isError, refetch } = useApiQuery(ApiRouter.quizRoomsId.get, { params: [id] });
  const toast = useToast();

  useEffect(() => {
    const id = router.query.id;
    if (id && typeof id === 'string') {
      setId(+id);
      socketService.publish(SocketEvent.quizRoomUpdated, { type: 'join', id: +id });
      socketService.subscribe(SocketEvent.quizRoomUpdated, async (payload) => {
        console.log(payload);
        if (payload.type === 'refetch') {
          await refetch();
        }

        if (payload.type === 'start') {
          toast({
            title: '잠시 후 게임이 시작됩니다.',
            status: 'loading',
            position: 'top',
            duration: 3000,
            isClosable: true,
          });

          setTimeout(async () => await router.push('/advertises'), 3000);
        }
      });

      return () => socketService.unsubscribe(SocketEvent.quizRoomUpdated);
    }
  }, [refetch, router, router.query.id, toast]);

  if (isError) {
    router.push('/_error');
  }
  return (
    <VStack minHeight="100vh" spacing={4}>
      <Header id={id} />
      {currNavTab === 'quizSpace' && <QuizSpace id={id} />}
      {currNavTab === 'chatSpace' && <ChatSpace id={id} />}
      {currNavTab === 'scoreBoard' && <ScoreBoard id={id} />}
      {currNavTab === 'quizSetting' && <QuizSetting id={id} quizzes={quizzes} setQuizzes={setQuizzes} />}
      <Navigation id={id} currNavTab={currNavTab} setCurrNavTab={setCurrNavTab} />
    </VStack>
  );
};

export default QuizRoomsIdPage;
