import { ChakraProvider } from '@chakra-ui/react';
import { Layout } from '@danolja/web/components/layout/Index';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { useMemo } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

function CustomApp({ Component, pageProps }: AppProps) {
  const queryClient = useMemo(() => new QueryClient(), []);

  return (
    <>
      <Head>
        <title>Welcome to web!</title>
      </Head>
      <QueryClientProvider client={queryClient}>
        <ChakraProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </ChakraProvider>
        <ReactQueryDevtools />
      </QueryClientProvider>
    </>
  );
}

export default CustomApp;
