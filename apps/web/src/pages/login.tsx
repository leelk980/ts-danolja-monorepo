import { VStack } from '@chakra-ui/react';
import { GoogleButton } from '@danolja/web/components/login/GoogleButton';
import { Introduction } from '@danolja/web/components/login/Introduction';
import { KakaoButton } from '@danolja/web/components/login/KakaoButton';
import { NextPage } from 'next';

const LoginPage: NextPage = () => {
  return (
    <VStack justifyContent="center" minHeight="100vh" spacing={4}>
      <Introduction />
      <KakaoButton />
      <GoogleButton />
    </VStack>
  );
};

export default LoginPage;
