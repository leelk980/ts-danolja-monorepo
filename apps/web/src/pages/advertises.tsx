import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

const AdvertisesPage: NextPage = () => {
  const router = useRouter();
  useEffect(() => {
    setTimeout(() => router.back(), 3000);
  }, [router]);

  return <div>서버 동기화 중... </div>;
};

export default AdvertisesPage;
