import { ApiRouter } from '@danolja/shared';
import { LocalStorageEnum } from '@danolja/web/commons/enums/storage.enum';
import { useApiInvalidation } from '@danolja/web/hooks/useApiInvalidation';
import { useApiMutation } from '@danolja/web/hooks/useApiMutation';
import { storageService } from '@danolja/web/services/storage.service';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';

const SsoPage: NextPage = () => {
  const router = useRouter();
  const { mutateAsync } = useApiMutation(ApiRouter.signIn.post);
  const { invalidateAsync } = useApiInvalidation(ApiRouter.userMe.get);

  const signIn = useCallback(async () => {
    if (!router.query) return;

    const { email } = router.query;

    if (typeof email === 'string') {
      const res = await mutateAsync({ email });

      const { accessToken, refreshToken } = res;

      storageService.local.set(LocalStorageEnum.ACCESS_TOKEN, accessToken);
      storageService.local.set(LocalStorageEnum.REFRESH_TOKEN, refreshToken);

      await invalidateAsync();
    }
  }, [router.query, mutateAsync, invalidateAsync]);

  useEffect(() => {
    signIn().then(() => router.push('/'));
  }, [router, signIn]);

  return <div></div>;
};

export default SsoPage;
