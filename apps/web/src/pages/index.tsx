import { VStack } from '@chakra-ui/react';
import { CreateRoomButton } from '@danolja/web/components/index/CreateRoomButton';
import { Header } from '@danolja/web/components/index/Header';
import { Introduction } from '@danolja/web/components/index/Introduction';
import { JoinRoomButton } from '@danolja/web/components/index/JoinRoomButton';
import { NextPage } from 'next';

const IndexPage: NextPage = () => {
  return (
    <>
      <Header />
      <VStack justifyContent="center" minHeight="100vh" spacing={4}>
        <Introduction />
        <CreateRoomButton />
        <JoinRoomButton />
      </VStack>
    </>
  );
};

export default IndexPage;
