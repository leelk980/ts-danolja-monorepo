import { LocalStorageEnum } from '@danolja/web/commons/enums/storage.enum';

class StorageService {
  local = {
    get(key: LocalStorageEnum) {
      return localStorage.getItem(key);
    },

    set(key: LocalStorageEnum, value: string) {
      localStorage.setItem(key, value);
    },

    remove(key: LocalStorageEnum) {
      localStorage.removeItem(key);
    },
  };

  session = {};

  cookie = {};
}

export const storageService = new StorageService();
