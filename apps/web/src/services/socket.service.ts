import { BaseSocketEvent } from '@danolja/shared';
import { io, Socket } from 'socket.io-client';

class SocketService {
  constructor(private readonly socket: Socket) {}

  subscribe<T extends BaseSocketEvent<unknown>>(event: T, cb: (payload: T['payload']) => unknown | Promise<unknown>) {
    this.socket.on(event.name, cb);
  }

  publish<T extends BaseSocketEvent<unknown>>(event: T, data: T['payload']) {
    this.socket.emit(event.name, data);
  }

  unsubscribe<T extends BaseSocketEvent<unknown>>(events: T | T[]) {
    if (!Array.isArray(events)) {
      this.socket.off(events.name);
      return;
    }

    for (const event of events) {
      this.socket.off(event.name);
    }
  }
}

const url = (process.env.NEXT_PUBLIC_API_URL ?? '').split('/api')[0];
const socket = io(url);

export const socketService = new SocketService(socket);
