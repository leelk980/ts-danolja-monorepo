import { LocalStorageEnum } from '@danolja/web/commons/enums/storage.enum';
import { storageService } from '@danolja/web/services/storage.service';
import axios, { Axios, AxiosInstance, AxiosRequestConfig, AxiosStatic } from 'axios';

class HttpService {
  request: (config: AxiosRequestConfig) => Promise<unknown>;
  requestApi: (config: AxiosRequestConfig) => Promise<unknown>;

  constructor(options: { axiosStatic: AxiosStatic; axiosInstance: AxiosInstance }) {
    this.request = this.catchAsync(options.axiosStatic.request);
    this.requestApi = this.catchAsync(options.axiosInstance.request);
  }

  // TODO: 예외처리
  private catchAsync(axiosRequest: Axios['request']) {
    return (config: AxiosRequestConfig) =>
      axiosRequest(config)
        .then((res) => res.data)
        .catch((e) => {
          throw new Error(e);
        });
  }
}

export const httpService = new HttpService({
  axiosStatic: axios,
  axiosInstance: (() => {
    const instance = axios.create({
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      headers: { 'Content-Type': 'application/json' },
    });

    instance.interceptors.request.use(
      (config) => {
        if (window === undefined) {
          return config;
        }

        const accessToken = storageService.local.get(LocalStorageEnum.ACCESS_TOKEN);
        if (accessToken && config.headers) {
          config.headers.Authorization = `Bearer ${accessToken}`;
        }

        return config;
      },
      (err) => Promise.reject(err),
    );

    return instance;
  })(),
});
