import { Heading, Table, TableCaption, TableContainer, Tbody, Td, Th, Thead, Tr, VStack } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';

export const ScoreBoard = (props: { id: number }) => {
  const { data: userMeData } = useApiQuery(ApiRouter.userMe.get);
  const { data } = useApiQuery(ApiRouter.quizRoomsId.get, { params: [props.id] });

  return (
    <VStack spacing={4} width="80%">
      <Heading>ScoreBoard</Heading>
      <TableContainer width="100%">
        <Table variant="simple">
          <Thead>
            <Tr>
              <Th>방장</Th>
              <Th>닉네임</Th>
              <Th>점수</Th>
            </Tr>
          </Thead>
          <Tbody>
            {data?.quizRoomUsers.map((quizRoomUser) => (
              <Tr key={quizRoomUser.id}>
                <Td>{quizRoomUser.isOwner ? 'O' : 'X'}</Td>
                <Td>
                  {quizRoomUser.userId === userMeData?.id && '* '}
                  {quizRoomUser.nickname}
                </Td>
                <Td>{quizRoomUser.totalScore}</Td>
              </Tr>
            ))}
          </Tbody>
          <TableCaption textAlign="left">* : 나의 점수</TableCaption>
        </Table>
      </TableContainer>
    </VStack>
  );
};
