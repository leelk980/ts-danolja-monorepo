import { Button, FormControl, Heading, Input, Text, VStack } from '@chakra-ui/react';
import { ApiRouter, SocketEvent } from '@danolja/shared';
import { useApiMutation } from '@danolja/web/hooks/useApiMutation';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { socketService } from '@danolja/web/services/socket.service';
import { KeyboardEventHandler, useCallback, useLayoutEffect, useMemo, useRef } from 'react';

export const ChatSpace = (props: { id: number }) => {
  const { data: userMeData } = useApiQuery(ApiRouter.userMe.get);
  const { data } = useApiQuery(ApiRouter.quizRoomsId.get, { params: [props.id] });
  const { mutateAsync } = useApiMutation(ApiRouter.quizRoomsIdChats.post, { params: [props.id] });

  const chatContainerRef = useRef<HTMLDivElement>(null);
  const messageInputRef = useRef<HTMLInputElement>(null);
  const quizRoomUserMap = useMemo(() => data?.quizRoomUsers.associateBy('id'), [data?.quizRoomUsers]);

  useLayoutEffect(() => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
    }
  }, [data?.quizRoomChats.length]);

  const handleMessageSubmitClick = useCallback(async () => {
    const messageInputValue = messageInputRef?.current?.value;
    if (!messageInputValue) {
      return;
    }

    await mutateAsync({ message: messageInputValue });
    socketService.publish(SocketEvent.quizRoomUpdated, { id: props.id, type: 'refetch' });
    messageInputRef.current.value = '';
  }, [mutateAsync, props.id]);

  const handleMessageInputEnterKeyDown: KeyboardEventHandler<HTMLInputElement> = useCallback(
    async (event) => {
      if (event.key === 'Enter') {
        event.preventDefault();
        await handleMessageSubmitClick();
      }
    },
    [handleMessageSubmitClick],
  );

  return (
    <VStack spacing={4} width="80%">
      <Heading>ChatSpace</Heading>
      <VStack ref={chatContainerRef} width="100%" height={600} spacing={4} overflowY="scroll">
        {quizRoomUserMap &&
          data?.quizRoomChats.map((quizRoomChat) => {
            const { id, quizRoomUserId, message, isBot, createdAt } = quizRoomChat;

            if (isBot) {
              return (
                <VStack key={id} backgroundColor="whitesmoke" padding={4} width="80%">
                  <Text>{`채팅봇 - ${createdAt}`}</Text>
                  <Text>{message}</Text>
                </VStack>
              );
            }

            if (quizRoomUserMap[quizRoomUserId].userId === userMeData?.id) {
              return (
                <VStack key={id} backgroundColor="whatsapp.600" padding={4} width="80%">
                  <Text>{`나 - ${createdAt}`}</Text>
                  <Text>{message}</Text>
                </VStack>
              );
            }

            return (
              <VStack key={id} backgroundColor="yellow.100" padding={4} width="80%">
                <Text>{`${quizRoomUserMap[quizRoomUserId].nickname} - ${createdAt}`}</Text>
                <Text>{message}</Text>
              </VStack>
            );
          })}
      </VStack>

      <FormControl width="100%" display="flex" paddingTop={12}>
        <Input placeholder="메시지를 입력해주세요." ref={messageInputRef} onKeyDown={handleMessageInputEnterKeyDown} />
        <Button type="submit" paddingX={8} onClick={handleMessageSubmitClick}>
          전송
        </Button>
      </FormControl>
    </VStack>
  );
};
