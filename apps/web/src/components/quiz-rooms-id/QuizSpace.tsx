import { Heading, HStack, Text, VStack } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';

export const QuizSpace = (props: { id: number }) => {
  const { data } = useApiQuery(ApiRouter.quizRoomsId.get, { params: [props.id] });

  if (!data?.currentQuiz) {
    return (
      <VStack spacing={4} width="80%">
        <Heading marginBottom={200}>QuizSpace</Heading>
        <Text>시작 대기중 입니다.</Text>
        <Text>잠시만 기다려주세요.</Text>
      </VStack>
    );
  }

  return (
    <VStack spacing={4} width="80%">
      <Heading>QuizSpace</Heading>
      <VStack width="100%" spacing={8}>
        <HStack spacing={4}>
          <Heading size="md">제목</Heading>
          <Text>{data.currentQuiz.title}</Text>
          <Heading size="md">부제</Heading>
          <Text>{data.currentQuiz.subTitle}</Text>
        </HStack>

        <HStack spacing={4}>
          <Heading size="md">점수</Heading>
          <Text>{data.currentQuiz.score}</Text>
          <Heading size="md">타입</Heading>
          <Text>{data.currentQuiz.type}</Text>
        </HStack>

        {data.currentQuiz.quizCategories.map((quizCategory, index) => {
          const { id, name, description } = quizCategory;

          return (
            <HStack key={id} spacing={4}>
              <Heading size="md">{`카테고리 ${index + 1}`}</Heading>
              <Text>{`이름: ${name}`}</Text>
              <Text>{`설명: ${description}`}</Text>
            </HStack>
          );
        })}

        {data.currentQuiz.quizOptions.map((quizOption, index) => {
          const { id, title, subTitle } = quizOption;

          return (
            <HStack key={id} spacing={4}>
              <Heading size="md">{`선택지 ${index + 1}`}</Heading>
              <Text>{`이름: ${title}`}</Text>
              <Text>{`설명: ${subTitle}`}</Text>
            </HStack>
          );
        })}
      </VStack>
    </VStack>
  );
};
