import {
  Button,
  Heading,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react';
import { Quiz } from '@danolja/web/components/quiz-rooms-id/QuizSetting/index';
import { Dispatch, MouseEventHandler, SetStateAction, useCallback } from 'react';

export const QuizList = (props: { quizzes: Quiz[]; setQuizzes: Dispatch<SetStateAction<Quiz[]>> }) => {
  const { quizzes, setQuizzes } = props;

  const handleQuizScoreButtonChange = useCallback(
    (quizId: number, score: number) =>
      setQuizzes((prev) => {
        const target = prev.find((quiz) => quiz.id === quizId);
        if (target) {
          target.score = score;
        }

        return prev;
      }),
    [setQuizzes],
  );

  const handleQuizDeleteButtonClick: MouseEventHandler<HTMLButtonElement> = useCallback(
    (event) => {
      event.preventDefault();
      setQuizzes((prev) => prev.filter((quiz) => quiz.id !== +(event.target as HTMLButtonElement).value));
    },
    [setQuizzes],
  );

  return (
    <TableContainer width="100%" height={480} overflowY="scroll">
      <Heading width="100%" marginY={2} size="sm" textAlign="center">
        퀴즈목록
      </Heading>
      <Table variant="simple">
        <Thead>
          <Tr>
            <Th>순서</Th>
            <Th>제목</Th>
            <Th>타입</Th>
            <Th>점수</Th>
            <Th>삭제</Th>
          </Tr>
        </Thead>
        <Tbody>
          {quizzes.map((quiz, index) => (
            <Tr key={quiz.id} fontSize={12}>
              <Td>{index + 1}</Td>
              <Td>{quiz.title}</Td>
              <Td>{quiz.type}</Td>
              <Td>
                <NumberInput
                  defaultValue={quiz.score}
                  min={1}
                  max={9}
                  size="xs"
                  onChange={(_, valueAsNumber) => handleQuizScoreButtonChange(quiz.id, valueAsNumber)}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
              </Td>
              <Td>
                <Button value={quiz.id} size="xs" colorScheme="red" onClick={handleQuizDeleteButtonClick}>
                  X
                </Button>
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
};
