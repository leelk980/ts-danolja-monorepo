import { FormControl, FormLabel, Select } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { Quiz } from '@danolja/web/components/quiz-rooms-id/QuizSetting/index';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { ChangeEventHandler, Dispatch, SetStateAction, useCallback } from 'react';

export const QuizCategorySelect = (props: {
  setCurrQuizCategoryId: Dispatch<SetStateAction<number>>;
  setCurrQuiz: Dispatch<SetStateAction<Quiz | null>>;
}) => {
  const { setCurrQuizCategoryId, setCurrQuiz } = props;
  const { data } = useApiQuery(ApiRouter.categories.get);

  const handleCategorySelectChange: ChangeEventHandler<HTMLSelectElement> = useCallback(
    (event) => {
      event.preventDefault();
      setCurrQuizCategoryId(+event.target.value);
      setCurrQuiz(null);
    },
    [setCurrQuizCategoryId, setCurrQuiz],
  );

  return (
    <FormControl>
      <FormLabel>퀴즈 카테고리</FormLabel>
      <Select placeholder="선택해주세요" onChange={handleCategorySelectChange}>
        {data?.categories.map((category) => (
          <option key={category.id} value={category.id}>{`${category.name} (${category.description})`}</option>
        ))}
      </Select>
    </FormControl>
  );
};
