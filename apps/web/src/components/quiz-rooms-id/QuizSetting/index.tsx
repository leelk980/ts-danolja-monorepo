import { Heading, VStack } from '@chakra-ui/react';
import { ActionButtons } from '@danolja/web/components/quiz-rooms-id/QuizSetting/ActionButtons';
import { QuizCategorySelect } from '@danolja/web/components/quiz-rooms-id/QuizSetting/QuizCategorySelect';
import { QuizDetail } from '@danolja/web/components/quiz-rooms-id/QuizSetting/QuizDetail';
import { QuizList } from '@danolja/web/components/quiz-rooms-id/QuizSetting/QuizList';
import { QuizSelect } from '@danolja/web/components/quiz-rooms-id/QuizSetting/QuizSelect';
import { Dispatch, SetStateAction, useState } from 'react';

export type Quiz = {
  id: number;
  title: string;
  type: string;
  score: number;
};

export const QuizSetting = (props: { id: number; quizzes: Quiz[]; setQuizzes: Dispatch<SetStateAction<Quiz[]>> }) => {
  const { id, quizzes, setQuizzes } = props;
  const [currQuizCategoryId, setCurrQuizCategoryId] = useState(0);
  const [currQuiz, setCurrQuiz] = useState<Quiz | null>(null);
  const [showDetail, setShowDetail] = useState(true);

  return (
    <VStack spacing={4} width="80%">
      <Heading>QuizSetting</Heading>
      <QuizCategorySelect setCurrQuizCategoryId={setCurrQuizCategoryId} setCurrQuiz={setCurrQuiz} />
      <QuizSelect currQuizCategoryId={currQuizCategoryId} setCurrQuiz={setCurrQuiz} />
      {showDetail ? <QuizDetail currQuiz={currQuiz} /> : <QuizList quizzes={quizzes} setQuizzes={setQuizzes} />}
      <ActionButtons
        quizRoomId={id}
        showDetail={showDetail}
        setShowDetail={setShowDetail}
        currQuiz={currQuiz}
        quizzes={quizzes}
        setQuizzes={setQuizzes}
      />
    </VStack>
  );
};
