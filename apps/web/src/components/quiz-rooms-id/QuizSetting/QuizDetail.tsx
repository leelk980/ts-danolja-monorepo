import { Heading, Text, VStack } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { Quiz } from '@danolja/web/components/quiz-rooms-id/QuizSetting/index';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';

export const QuizDetail = (props: { currQuiz: Quiz | null }) => {
  const { currQuiz } = props;
  const { data } = useApiQuery(ApiRouter.quizzesId.get, { params: [currQuiz?.id] });

  if (!data) {
    return (
      <VStack width="100%" height={480} overflowY="scroll" spacing={4}>
        <Heading marginY={2} size="sm">
          상세설명
        </Heading>
        <Text position="relative" bottom="-28%">
          선택된 퀴즈가 없습니다.
        </Text>
      </VStack>
    );
  }

  return (
    <VStack width="100%" height={480} overflowY="scroll" spacing={4}>
      <Heading marginY={2} size="sm">
        상세설명
      </Heading>
      <Text width="100%">{`제목: ${data?.title}`}</Text>
      <Text width="100%">{`부제: ${data?.subTitle}`}</Text>
      <Text width="100%">{`타입: ${data?.type}`}</Text>
      <Text width="100%">{`카테고리: ${data?.quizCategories.map(
        (each) => `${each.name} (${each.description})`,
      )}`}</Text>
      <Text width="100%">{`옵션: ${data?.quizOptions.map(
        (each) => `${each.title} (${each.subTitle}) ${each.isCorrect && '*정답'}`,
      )}`}</Text>
    </VStack>
  );
};
