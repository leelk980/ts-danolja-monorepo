import { FormControl, FormLabel, Select } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { Quiz } from '@danolja/web/components/quiz-rooms-id/QuizSetting/index';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { ChangeEventHandler, Dispatch, SetStateAction, useCallback } from 'react';

export const QuizSelect = (props: {
  currQuizCategoryId: number;
  setCurrQuiz: Dispatch<SetStateAction<Quiz | null>>;
}) => {
  const { currQuizCategoryId, setCurrQuiz } = props;
  const { data } = useApiQuery(ApiRouter.quizzes.get, { queryString: { categoryId: currQuizCategoryId } });

  const handleQuizSelectChange: ChangeEventHandler<HTMLSelectElement> = useCallback(
    (event) => {
      event.preventDefault();
      const quiz = data?.quizzes.find((quiz) => quiz.id === +event.target.value);
      setCurrQuiz(quiz ? { ...quiz, score: 1 } : null);
    },
    [data?.quizzes, setCurrQuiz],
  );

  return (
    <FormControl>
      <FormLabel>퀴즈 선택</FormLabel>
      <Select placeholder="선택해주세요" onChange={handleQuizSelectChange}>
        {data?.quizzes.map((quiz) => (
          <option key={quiz.id} value={quiz.id}>{`${quiz.title}`}</option>
        ))}
      </Select>
    </FormControl>
  );
};
