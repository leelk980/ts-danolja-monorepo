import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  HStack,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import { ApiRouter, SocketEvent } from '@danolja/shared';
import { Quiz } from '@danolja/web/components/quiz-rooms-id/QuizSetting/index';
import { useApiMutation } from '@danolja/web/hooks/useApiMutation';
import { socketService } from '@danolja/web/services/socket.service';
import { Dispatch, MouseEventHandler, SetStateAction, useCallback, useRef } from 'react';

export const ActionButtons = (props: {
  quizRoomId: number;
  showDetail: boolean;
  setShowDetail: Dispatch<SetStateAction<boolean>>;
  currQuiz: Quiz | null;
  quizzes: Quiz[];
  setQuizzes: Dispatch<SetStateAction<Quiz[]>>;
}) => {
  const { quizRoomId, showDetail, setShowDetail, currQuiz, quizzes, setQuizzes } = props;

  const toast = useToast();
  const { mutateAsync } = useApiMutation(ApiRouter.quizRoomsId.patch, { params: [quizRoomId] });
  const { isOpen, onOpen, onClose } = useDisclosure();
  const startModalCancelButtonRef = useRef<HTMLButtonElement>(null);

  const handleQuizAddButtonClick: MouseEventHandler<HTMLButtonElement> = useCallback(
    (event) => {
      event.preventDefault();
      setQuizzes((prev) => {
        if (!currQuiz) {
          toast({
            title: '퀴즈를 선택해주세요.',
            status: 'error',
            duration: 3000,
            isClosable: true,
          });
          return prev;
        }
        if (prev.map((quiz) => quiz.id).includes(currQuiz.id)) {
          toast({
            title: '이미 선택된 항목입니다.',
            status: 'error',
            duration: 3000,
            isClosable: true,
          });
          return prev;
        }

        toast({
          title: '추가되었습니다.',
          status: 'success',
          duration: 3000,
          isClosable: true,
        });
        return [...prev, currQuiz];
      });
    },
    [currQuiz, setQuizzes, toast],
  );

  const handleStartButtonClick: MouseEventHandler<HTMLButtonElement> = useCallback(async () => {
    if (quizzes.length === 0) {
      toast({
        title: '1개 이상의 퀴즈를 선택해주세요.',
        status: 'error',
        duration: 3000,
        isClosable: true,
      });

      return onClose();
    }

    await mutateAsync(
      {
        quizRoomQuizzes: quizzes.map((quiz) => ({
          id: quiz.id,
          score: quiz.score,
        })),
      },
      {
        onSuccess: async () => {
          socketService.publish(SocketEvent.quizRoomUpdated, { type: 'start', id: quizRoomId });
        },
        onError: () => {
          toast({
            title: '게임을 시작할 수 없습니다.',
            status: 'error',
            duration: 3000,
            isClosable: true,
          });
        },
        onSettled: () => onClose(),
      },
    );
  }, [mutateAsync, onClose, quizRoomId, quizzes, toast]);

  return (
    <HStack width="100%" justifyContent="flex-end" spacing={4} marginTop={40}>
      <Button colorScheme="blue" onClick={() => setShowDetail((prev) => !prev)}>
        {showDetail ? '목록보기' : '상세정보'}
      </Button>
      <Button colorScheme="green" onClick={handleQuizAddButtonClick}>
        추가하기
      </Button>
      <Button colorScheme="red" onClick={onOpen}>
        시작하기
      </Button>
      <AlertDialog isOpen={isOpen} leastDestructiveRef={startModalCancelButtonRef} onClose={onClose}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              게임시작
            </AlertDialogHeader>
            <AlertDialogBody>시작하시겠습니까?</AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={startModalCancelButtonRef} onClick={onClose}>
                취소
              </Button>
              <Button colorScheme="linkedin" onClick={handleStartButtonClick} ml={3}>
                시작
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </HStack>
  );
};
