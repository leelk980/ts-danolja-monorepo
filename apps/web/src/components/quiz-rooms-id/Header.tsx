import { Heading, HStack } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';

export const Header = (props: { id: number }) => {
  const { data } = useApiQuery(ApiRouter.quizRoomsId.get, { params: [props.id] });

  return (
    <HStack width="100%" padding={4} justifyContent="space-between" backgroundColor="whatsapp.100">
      <Heading size="sm">[이름] {data?.name}</Heading>
      <Heading size="sm">[상태] {data?.status}</Heading>
      <Heading size="sm">[입장코드] {data?.accessCode}</Heading>Heading
    </HStack>
  );
};
