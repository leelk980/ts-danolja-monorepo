import { Button, HStack } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { Dispatch, SetStateAction, useCallback, useMemo } from 'react';

export type NavigationTab = 'quizSpace' | 'chatSpace' | 'scoreBoard' | 'quizSetting' | 'quizList';

export const Navigation = (props: {
  id: number;
  currNavTab: NavigationTab;
  setCurrNavTab: Dispatch<SetStateAction<NavigationTab>>;
}) => {
  const { id, currNavTab, setCurrNavTab } = props;
  const { data } = useApiQuery(ApiRouter.quizRoomsId.get, { params: [id] });
  const { data: userMeData } = useApiQuery(ApiRouter.userMe.get);

  const handleButtonColorScheme = useCallback(
    (tab: NavigationTab) => (currNavTab === tab ? 'blue' : 'gray'),
    [currNavTab],
  );

  const isCurrUserOwner = useMemo(
    () => data?.quizRoomUsers.find((quizRoomUser) => quizRoomUser.userId === userMeData?.id)?.isOwner,
    [data?.quizRoomUsers, userMeData?.id],
  );

  return (
    <HStack
      position="fixed"
      bottom={0}
      width={600}
      padding={4}
      justifyContent="space-between"
      backgroundColor="whatsapp.100"
    >
      <Button onClick={() => setCurrNavTab('quizSpace')} colorScheme={handleButtonColorScheme('quizSpace')}>
        quizSpace
      </Button>
      <Button onClick={() => setCurrNavTab('chatSpace')} colorScheme={handleButtonColorScheme('chatSpace')}>
        chatSpace
      </Button>
      <Button onClick={() => setCurrNavTab('scoreBoard')} colorScheme={handleButtonColorScheme('scoreBoard')}>
        scoreBoard
      </Button>
      {isCurrUserOwner &&
        (data?.status === 'ready' ? (
          <Button onClick={() => setCurrNavTab('quizSetting')} colorScheme={handleButtonColorScheme('quizSetting')}>
            quizSetting
          </Button>
        ) : (
          <Button onClick={() => setCurrNavTab('quizList')} colorScheme={handleButtonColorScheme('quizList')}>
            quizList
          </Button>
        ))}
    </HStack>
  );
};
