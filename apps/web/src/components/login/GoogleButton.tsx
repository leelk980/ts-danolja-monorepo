import { Button } from '@chakra-ui/react';

export const GoogleButton = () => {
  return (
    <Button width={400} backgroundColor="gray.300" _hover={{ bgColor: 'gray.400' }}>
      구글 로그인
    </Button>
  );
};
