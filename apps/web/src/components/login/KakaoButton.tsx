import { Button } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import Link from 'next/link';

export const KakaoButton = () => {
  return (
    <Link href={`${process.env.NEXT_PUBLIC_API_URL}${ApiRouter.ssoKakao.get.uri}`}>
      <Button width={400} backgroundColor="yellow.300" _hover={{ bgColor: 'yellow.400' }}>
        카카오 로그인
      </Button>
    </Link>
  );
};
