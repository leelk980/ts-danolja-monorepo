import {
  Button,
  FormControl,
  FormLabel,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { useApiMutation } from '@danolja/web/hooks/useApiMutation';
import { useRouter } from 'next/router';
import { MouseEventHandler, useCallback, useRef } from 'react';

export const CreateRoomButton = () => {
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { mutateAsync } = useApiMutation(ApiRouter.quizRooms.post);
  const quizRoomNameRef = useRef<HTMLInputElement>(null);
  const nicknameRef = useRef<HTMLInputElement>(null);

  const handleCreateButtonClick: MouseEventHandler = useCallback(
    async (event) => {
      event.preventDefault();
      const quizRoomName = quizRoomNameRef?.current?.value;
      const nickname = nicknameRef?.current?.value;

      if (!quizRoomName || !nickname) {
        return alert('모든 항목을 입력해주세요.');
      }

      await mutateAsync(
        { quizRoomName, nickname },
        {
          onError: (err) => alert(err),
          onSuccess: (id) => router.push(`/quiz-rooms/${id}`),
        },
      );
    },
    [mutateAsync, router],
  );

  return (
    <HStack>
      <Button width={400} height={100} backgroundColor="gray.300" _hover={{ bgColor: 'gray.400' }} onClick={onOpen}>
        생성하기
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} initialFocusRef={quizRoomNameRef}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>방만들기</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <VStack spacing={4} marginY={4}>
              <FormControl isRequired>
                <FormLabel>방이름</FormLabel>
                <Input placeholder="방의 이름을 입력해주세요." ref={quizRoomNameRef} />
              </FormControl>

              <FormControl isRequired>
                <FormLabel>닉네임</FormLabel>
                <Input placeholder="사용하실 닉네임을 입력해주세요." ref={nicknameRef} />
              </FormControl>
            </VStack>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleCreateButtonClick}>
              생성하기
            </Button>
            <Button onClick={onClose}>취소하기</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </HStack>
  );
};
