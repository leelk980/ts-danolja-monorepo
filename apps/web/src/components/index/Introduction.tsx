import { Heading, VStack } from '@chakra-ui/react';

export const Introduction = () => {
  return (
    <VStack spacing={12} marginTop={-12} marginBottom={12}>
      <Heading size="2xl">서비스명</Heading>
      <Heading size="md">서비스 소개</Heading>
    </VStack>
  );
};
