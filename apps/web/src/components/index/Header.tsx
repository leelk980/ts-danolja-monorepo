import { AddIcon, ExternalLinkIcon, HamburgerIcon } from '@chakra-ui/icons';
import {
  Button,
  Heading,
  HStack,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { LocalStorageEnum } from '@danolja/web/commons/enums/storage.enum';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { storageService } from '@danolja/web/services/storage.service';
import { useCallback } from 'react';

export const Header = () => {
  const { data, refetch } = useApiQuery(ApiRouter.userMe.get);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleLogoutButtonClick = useCallback(async () => {
    storageService.local.remove(LocalStorageEnum.ACCESS_TOKEN);
    storageService.local.remove(LocalStorageEnum.REFRESH_TOKEN);

    await onClose();
    await refetch();
  }, [onClose, refetch]);

  return (
    <HStack justifyContent="end" padding={4}>
      <Menu>
        <MenuButton as={IconButton} aria-label="Options" icon={<HamburgerIcon />} variant="outline" />
        <MenuList>
          <Heading padding={4} textAlign="right" size="sm">
            {data && data.name}
          </Heading>
          <MenuItem icon={<AddIcon />}>내 정보</MenuItem>

          <MenuItem icon={<ExternalLinkIcon />} onClick={onOpen}>
            로그아웃
          </MenuItem>
          <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>로그아웃</ModalHeader>
              <ModalCloseButton />
              <ModalBody>정말로 로그아웃하시겠습니까?</ModalBody>

              <ModalFooter>
                <Button colorScheme="blue" mr={4} onClick={handleLogoutButtonClick}>
                  네
                </Button>
                <Button onClick={onClose}>아니오</Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </MenuList>
      </Menu>
    </HStack>
  );
};
