import {
  Button,
  FormControl,
  FormHelperText,
  FormLabel,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { useApiMutation } from '@danolja/web/hooks/useApiMutation';
import { useRouter } from 'next/router';
import { MouseEventHandler, useCallback, useRef } from 'react';

export const JoinRoomButton = () => {
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { mutateAsync } = useApiMutation(ApiRouter.quizRooms.put);
  const accessCodeRef = useRef<HTMLInputElement>(null);
  const nicknameRef = useRef<HTMLInputElement>(null);

  const handleJoinButtonClick: MouseEventHandler = useCallback(
    async (event) => {
      event.preventDefault();
      const accessCode = accessCodeRef?.current?.value;
      const nickname = nicknameRef?.current?.value;

      if (!accessCode || !nickname) {
        return alert('모든 항목을 입력해주세요.');
      }

      await mutateAsync(
        { accessCode, nickname },
        {
          onError: (err) => alert(err),
          onSuccess: (id) => router.push(`/quiz-rooms/${id}`),
        },
      );
    },
    [mutateAsync, router],
  );

  return (
    <HStack>
      <Button width={400} height={100} backgroundColor="gray.300" _hover={{ bgColor: 'gray.400' }} onClick={onOpen}>
        참여하기
      </Button>

      <Modal isOpen={isOpen} onClose={onClose} initialFocusRef={accessCodeRef}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>참여하기</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <VStack spacing={4} marginY={4}>
              <FormControl isRequired>
                <FormLabel>입장코드</FormLabel>
                <Input placeholder="초대받은 입장 코드를 입력해주세요." ref={accessCodeRef} />
              </FormControl>

              <FormControl isRequired>
                <FormLabel>닉네임</FormLabel>
                <Input placeholder="사용하실 닉네임을 입력해주세요." ref={nicknameRef} />
                <FormHelperText marginTop={2} fontSize={12}>
                  등록된 사용자의 경우 이전의 닉네임이 유지됩니다.
                </FormHelperText>
              </FormControl>
            </VStack>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleJoinButtonClick}>
              참여하기
            </Button>
            <Button onClick={onClose}>취소하기</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </HStack>
  );
};
