import { Text } from '@chakra-ui/react';

export const Loading = () => {
  return <Text padding={2}>Loading...</Text>;
};
