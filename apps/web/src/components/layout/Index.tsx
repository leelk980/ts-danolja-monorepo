import { Box } from '@chakra-ui/react';
import { ApiRouter } from '@danolja/shared';
import { Loading } from '@danolja/web/components/layout/Loading';
import { useApiQuery } from '@danolja/web/hooks/useApiQuery';
import { useRouter } from 'next/router';
import { PropsWithChildren, useEffect, useMemo, useState } from 'react';

export const Layout = (props: PropsWithChildren) => {
  const router = useRouter();
  const [currentPage, setCurrentPage] = useState('/');
  const { isError, isSuccess } = useApiQuery(ApiRouter.userMe.get);
  const publicPages = useMemo(() => ['/login', '/_error', '/_sso'], []);

  useEffect(() => {
    const currentPage = router.pathname;
    setCurrentPage(currentPage);

    if (isError && !publicPages.includes(currentPage)) {
      router.push('/login');
    }
  }, [isError, publicPages, router]);

  return (
    <Box width={600} minHeight="100vh" marginX="auto" backgroundColor="blue.100">
      {isSuccess || publicPages.includes(currentPage) ? props.children : <Loading />}
    </Box>
  );
};
