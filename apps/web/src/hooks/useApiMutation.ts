import { ServerApi } from '@danolja/shared';
import { LocalStorageEnum } from '@danolja/web/commons/enums/storage.enum';
import { buildUri } from '@danolja/web/commons/utils/build-uri';
import { httpService } from '@danolja/web/services/http.service';
import { storageService } from '@danolja/web/services/storage.service';
import { useMutation } from 'react-query';

export const useApiMutation = <T extends ServerApi & { method: 'post' | 'put' | 'patch' | 'delete' }>(
  api: T,
  options: { params?: unknown[] } = {},
) => {
  const { params } = options;

  return useMutation(async (data: T['body']) => {
    const url = buildUri(api.uri, params);
    const method = api.method;

    if (api.auth !== 'public' && !storageService.local.get(LocalStorageEnum.ACCESS_TOKEN)) {
      throw Error('invalid api mutation authentication');
    }

    return (await httpService.requestApi({ url, method, data })) as T['view'];
  });
};
