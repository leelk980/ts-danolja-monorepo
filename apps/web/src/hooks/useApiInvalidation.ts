import { ServerApi } from '@danolja/shared';
import { generateQueryKey } from '@danolja/web/commons/utils/generate-query-key';
import { useQueryClient } from 'react-query';

export const useApiInvalidation = <T extends ServerApi & { method: 'get' }>(
  api: T,
  options: { params?: unknown[]; queryString?: T['queryString'] } = {},
) => {
  const queryClient = useQueryClient();
  const queryKey = generateQueryKey(api, options);

  return {
    invalidateAsync: () => queryClient.invalidateQueries({ queryKey }),
  };
};
