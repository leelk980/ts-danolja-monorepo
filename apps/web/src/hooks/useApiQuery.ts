import { ServerApi } from '@danolja/shared';
import { LocalStorageEnum } from '@danolja/web/commons/enums/storage.enum';
import { generateQueryKey } from '@danolja/web/commons/utils/generate-query-key';
import { httpService } from '@danolja/web/services/http.service';
import { storageService } from '@danolja/web/services/storage.service';
import { useQuery } from 'react-query';

export const useApiQuery = <T extends ServerApi & { method: 'get' }>(
  api: T,
  options: { params?: unknown[]; queryString?: T['queryString']; refetchOnMount?: boolean } = {},
) => {
  const queryKey = generateQueryKey(api, options);

  return useQuery(
    queryKey,
    (context) => {
      const [builtUri, queryString] = context.queryKey;

      if (builtUri.split('').includes(':')) {
        throw Error(`invalid api query param: ${builtUri}`);
      }
      if (queryString && Object.values(queryString).filter((v) => !v).length > 0) {
        throw Error(`invalid api query queryString: ${queryString}`);
      }
      if (api.auth !== 'public' && !storageService.local.get(LocalStorageEnum.ACCESS_TOKEN)) {
        throw Error('invalid api query authentication');
      }

      return httpService.requestApi({
        url: builtUri,
        method: 'get',
        params: queryString,
      }) as T['view'];
    },
    { retry: 1, refetchOnMount: options?.refetchOnMount ?? false },
  );
};
