import { AppModule } from '@danolja/server/app.module';
import { environment } from '@danolja/server/common/environment/environment';
import { initApplication } from '@danolja/shared-server';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

async function bootstrap() {
  const application = await NestFactory.create(AppModule);
  const port = environment.port;

  await initApplication({ application, port });

  Logger.log(`🚀 Application is running on: http://localhost:${port}`);
}

bootstrap();
