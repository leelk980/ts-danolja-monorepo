import { CategoryRepository } from '@danolja/server/domain/core/repository/category.repository';
import { CategoryEntity, TypeormRepository } from '@danolja/shared-server';

export class CategoryTypeormRepository extends TypeormRepository<CategoryEntity> implements CategoryRepository {}
