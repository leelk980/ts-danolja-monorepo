import { UserRepository } from '@danolja/server/domain/user/repository/user.repository';
import { JoinEntity, TypeormRepository, UserEntity } from '@danolja/shared-server';
import { NotFoundException } from '@nestjs/common';

export class UserTypeormRepository extends TypeormRepository<UserEntity> implements UserRepository {
  async findOneByEmailWithSso(email: string): Promise<JoinEntity<UserEntity, 'userSso'>> {
    const entity = await this.createQueryBuilder('user')
      .leftJoinAndSelect('user.userSso', 'userSso')
      .where('user.email = :email', { email })
      .getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by email: ${email}`);
    }

    return entity as JoinEntity<UserEntity, 'userSso'>;
  }
}
