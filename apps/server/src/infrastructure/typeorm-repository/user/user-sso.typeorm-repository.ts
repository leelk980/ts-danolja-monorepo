import { UserSsoRepository } from '@danolja/server/domain/user/repository/user-sso.repository';
import { TypeormRepository, UserSsoEntity } from '@danolja/shared-server';

export class UserSsoTypeormRepository extends TypeormRepository<UserSsoEntity> implements UserSsoRepository {}
