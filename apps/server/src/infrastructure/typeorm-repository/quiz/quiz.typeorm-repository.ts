import { QuizRepository } from '@danolja/server/domain/quiz/repository/quiz.repository';
import { JoinEntity, QuizEntity, TypeormRepository } from '@danolja/shared-server';
import { NotFoundException } from '@nestjs/common';

export class QuizTypeormRepository extends TypeormRepository<QuizEntity> implements QuizRepository {
  async findManyByCategoryId(
    categoryId: number,
  ): Promise<JoinEntity<QuizEntity, never, 'quizOptions' | 'quizCategories'>[]> {
    return this.createQueryBuilder('quiz')
      .leftJoin('quiz.quizCategories', 'quizCategories')
      .where('quizCategories.categoryId = :categoryId', { categoryId })
      .orderBy('quiz.id', 'DESC')
      .getMany();
  }

  async findOneByIdWithJoin(id: number): Promise<JoinEntity<QuizEntity, 'quizOptions' | 'quizCategories'>> {
    const entity = await this.createQueryBuilder('quiz')
      .leftJoinAndSelect('quiz.quizCategories', 'quizCategories')
      .leftJoinAndSelect('quiz.quizOptions', 'quizOptions')
      .where('quiz.id = :id', { id })
      .getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by id: ${id}`);
    }

    return entity as JoinEntity<QuizEntity, 'quizOptions' | 'quizCategories'>;
  }
}
