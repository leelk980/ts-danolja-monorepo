import { QuizOptionRepository } from '@danolja/server/domain/quiz/repository/quiz-option.repository';
import { QuizOptionEntity, TypeormRepository } from '@danolja/shared-server';

export class QuizOptionTypeormRepository extends TypeormRepository<QuizOptionEntity> implements QuizOptionRepository {}
