import { QuizCategoryRepository } from '@danolja/server/domain/quiz/repository/quiz-category.repository';
import { QuizCategoryEntity, TypeormRepository } from '@danolja/shared-server';

export class QuizCategoryTypeormRepository
  extends TypeormRepository<QuizCategoryEntity>
  implements QuizCategoryRepository {}
