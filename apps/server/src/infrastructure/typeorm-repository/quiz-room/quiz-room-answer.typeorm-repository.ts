import { QuizRoomAnswerRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-answer.repository';
import { QuizRoomAnswerEntity, TypeormRepository } from '@danolja/shared-server';

export class QuizRoomAnswerTypeormRepository
  extends TypeormRepository<QuizRoomAnswerEntity>
  implements QuizRoomAnswerRepository {}
