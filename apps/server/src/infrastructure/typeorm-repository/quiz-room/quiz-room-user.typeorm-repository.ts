import { QuizRoomUserRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-user.repository';
import { QuizRoomUserEntity, TypeormRepository } from '@danolja/shared-server';

export class QuizRoomUserTypeormRepository
  extends TypeormRepository<QuizRoomUserEntity>
  implements QuizRoomUserRepository {}
