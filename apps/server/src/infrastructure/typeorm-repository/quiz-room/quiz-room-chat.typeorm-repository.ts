import { QuizRoomChatRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-chat.repository';
import { QuizRoomChatEntity, TypeormRepository } from '@danolja/shared-server';

export class QuizRoomChatTypeormRepository
  extends TypeormRepository<QuizRoomChatEntity>
  implements QuizRoomChatRepository {}
