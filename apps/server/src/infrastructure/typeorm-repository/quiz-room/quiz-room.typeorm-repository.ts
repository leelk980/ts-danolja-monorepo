import { QuizRoomRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room.repository';
import { JoinEntity, QuizRoomEntity, QuizRoomUserEntity, TypeormRepository } from '@danolja/shared-server';
import { NotFoundException } from '@nestjs/common';

export class QuizRoomTypeormRepository extends TypeormRepository<QuizRoomEntity> implements QuizRoomRepository {
  async findOneByIdWithUsers(id: number): Promise<JoinEntity<QuizRoomEntity, 'quizRoomUsers', 'quizRoomQuizzes'>> {
    const entity = await this.createQueryBuilder('quizRoom')
      .leftJoinAndSelect('quizRoom.quizRoomUsers', 'quizRoomUsers')
      .where('quizRoom.id = :id', { id })
      .getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by id: ${id}`);
    }

    return entity as JoinEntity<QuizRoomEntity, 'quizRoomUsers', 'quizRoomQuizzes'>;
  }

  async findOneByAccessCodeWithUsers(
    accessCode: string,
  ): Promise<JoinEntity<QuizRoomEntity, 'quizRoomUsers', 'quizRoomQuizzes'>> {
    const entity = await this.createQueryBuilder('quizRoom')
      .leftJoinAndSelect('quizRoom.quizRoomUsers', 'quizRoomUsers')
      .where('quizRoom.accessCode = :accessCode', { accessCode })
      .getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by accessCode: ${accessCode}`);
    }

    return entity as JoinEntity<QuizRoomEntity, 'quizRoomUsers', 'quizRoomQuizzes'>;
  }

  async findOneByIdWithUsersAndQuizzes(
    id: number,
  ): Promise<JoinEntity<QuizRoomEntity, 'quizRoomUsers' | 'quizRoomQuizzes'>> {
    const entity = await this.createQueryBuilder('quizRoom')
      .leftJoinAndSelect('quizRoom.quizRoomUsers', 'quizRoomUsers')
      .leftJoinAndSelect('quizRoom.quizRoomQuizzes', 'quizRoomQuizzes')
      .where('quizRoom.id = :id', { id })
      .getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by id: ${id}`);
    }

    return entity as JoinEntity<QuizRoomEntity, 'quizRoomUsers' | 'quizRoomQuizzes'>;
  }

  async findOneByIdWithQuizUserAnswerChats(id: number): Promise<
    JoinEntity<QuizRoomEntity, 'quizRoomQuizzes', 'quizRoomUsers'> & {
      quizRoomUsers: JoinEntity<QuizRoomUserEntity, 'quizRoomAnswers' | 'quizRoomChats', 'quizRoom'>[];
    }
  > {
    const entity = await this.createQueryBuilder('quizRoom')
      .leftJoinAndSelect('quizRoom.quizRoomUsers', 'quizRoomUsers')
      .leftJoinAndSelect('quizRoom.quizRoomQuizzes', 'quizRoomQuizzes')
      .leftJoinAndSelect('quizRoomUsers.quizRoomAnswers', 'quizRoomAnswers')
      .leftJoinAndSelect('quizRoomUsers.quizRoomChats', 'quizRoomChats')
      .where('quizRoom.id = :id', { id })
      .getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by id: ${id}`);
    }

    return entity as JoinEntity<QuizRoomEntity, 'quizRoomQuizzes', 'quizRoomUsers'> & {
      quizRoomUsers: JoinEntity<QuizRoomUserEntity, 'quizRoomAnswers' | 'quizRoomChats', 'quizRoom'>[];
    };
  }
}
