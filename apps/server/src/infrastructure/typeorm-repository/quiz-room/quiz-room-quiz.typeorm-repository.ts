import { QuizRoomQuizRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-quiz.repository';
import { QuizRoomQuizEntity, TypeormRepository } from '@danolja/shared-server';

export class QuizRoomQuizTypeormRepository
  extends TypeormRepository<QuizRoomQuizEntity>
  implements QuizRoomQuizRepository {}
