import { CoreApplicationService } from '@danolja/server/application/core.application-service';
import { QuizRoomApplicationService } from '@danolja/server/application/quiz-room.application-service';
import { QuizApplicationService } from '@danolja/server/application/quiz.application-service';
import { UserApplicationService } from '@danolja/server/application/user.application-service';
import { environment } from '@danolja/server/common/environment/environment';
import { KakaoGuard } from '@danolja/server/common/guard/kakao.guard';
import { LoginGuard } from '@danolja/server/common/guard/login.guard';
import { JwtService } from '@danolja/server/common/util/jwt.service';
import { CategoryRepository } from '@danolja/server/domain/core/repository/category.repository';
import { CoreCommandService } from '@danolja/server/domain/core/service/core.command-service';
import { CoreQueryService } from '@danolja/server/domain/core/service/core.query-service';
import { QuizRoomAnswerRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-answer.repository';
import { QuizRoomChatRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-chat.repository';
import { QuizRoomQuizRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-quiz.repository';
import { QuizRoomUserRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-user.repository';
import { QuizRoomRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room.repository';
import { QuizRoomCommandService } from '@danolja/server/domain/quiz-room/service/quiz-room.command-service';
import { QuizRoomQueryService } from '@danolja/server/domain/quiz-room/service/quiz-room.query-service';
import { QuizCategoryRepository } from '@danolja/server/domain/quiz/repository/quiz-category.repository';
import { QuizOptionRepository } from '@danolja/server/domain/quiz/repository/quiz-option.repository';
import { QuizRepository } from '@danolja/server/domain/quiz/repository/quiz.repository';
import { QuizCommandService } from '@danolja/server/domain/quiz/service/quiz.command-service';
import { QuizQueryService } from '@danolja/server/domain/quiz/service/quiz.query-service';
import { UserSsoRepository } from '@danolja/server/domain/user/repository/user-sso.repository';
import { UserRepository } from '@danolja/server/domain/user/repository/user.repository';
import { UserCommandService } from '@danolja/server/domain/user/service/user.command-service';
import { UserQueryService } from '@danolja/server/domain/user/service/user.query-service';
import { CoreController } from '@danolja/server/interface/core.controller';
import { QuizRoomController } from '@danolja/server/interface/quiz-room.controller';
import { QuizController } from '@danolja/server/interface/quiz.controller';
import { UserController } from '@danolja/server/interface/user.controller';
import { WsGateway } from '@danolja/server/interface/ws.gateway';
import { RepositoryModule } from '@danolja/shared-server';
import { Module, ValidationPipe } from '@nestjs/common';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClsModule } from 'nestjs-cls';

const controllers = [CoreController, QuizController, QuizRoomController, UserController];
const gateways = [WsGateway];

const applicationServices = [
  CoreApplicationService,
  QuizApplicationService,
  QuizRoomApplicationService,
  UserApplicationService,
];

const domainServices = [
  // Core
  CoreCommandService,
  CoreQueryService,
  // Quiz
  QuizCommandService,
  QuizQueryService,
  // QuizRoom
  QuizRoomCommandService,
  QuizRoomQueryService,
  // User
  UserCommandService,
  UserQueryService,
];

const [entities, repositoryImpls] = RepositoryModule.register(
  // Core
  CategoryRepository,
  // Quiz
  QuizRepository,
  QuizCategoryRepository,
  QuizOptionRepository,
  // QuizRoom
  QuizRoomRepository,
  QuizRoomChatRepository,
  QuizRoomUserRepository,
  QuizRoomQuizRepository,
  QuizRoomAnswerRepository,
  // User
  UserRepository,
  UserSsoRepository,
);

const commons = [KakaoGuard, JwtService];

@Module({
  imports: [
    ClsModule.forRoot({
      global: true,
      middleware: { mount: true },
    }),
    TypeOrmModule.forRoot(environment.typeorm(entities)),
  ],
  controllers,
  providers: [
    ...gateways,
    ...applicationServices,
    ...domainServices,
    ...repositoryImpls,
    ...commons,
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true,
        transform: true,
        transformOptions: {
          enableImplicitConversion: true,
        },
      }),
    },
    {
      provide: APP_GUARD,
      useClass: LoginGuard,
    },
  ],
})
export class AppModule {}
