import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export interface Environment {
  isProduction: boolean;
  port: number;
  webUrl: string;
  serverUrl: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  typeorm: (entities: any[]) => TypeOrmModuleOptions;
  kakaoSso: {
    clientId: string;
    clientSecret: string;
  };
  jwtSecret: string;
}

export const environment = {} as Environment;
