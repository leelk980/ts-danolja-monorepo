import { Environment } from '@danolja/server/common/environment/environment';

export const environment: Environment = {
  isProduction: false,
  port: 3001,
  webUrl: 'http://localhost:4200',
  serverUrl: 'http://localhost:3001/api',
  typeorm: (entities) => ({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root1234',
    database: 'danolja_local',
    synchronize: true,
    logging: true,
    entities,
  }),
  kakaoSso: {
    clientId: '57918a024c3f67fea70bc5eaf51b51a7',
    clientSecret: 'R50zD42f9PgM8SNUpxcxBWPAhGCnvhsS',
  },
  jwtSecret: 'jwt',
};
