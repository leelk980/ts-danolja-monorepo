import { Environment } from '@danolja/server/common/environment/environment';

export const environment: Environment = {
  isProduction: true,
  port: 4001,
  webUrl: 'https://danolja.dev',
  serverUrl: 'https://danolja.dev/api',
  typeorm: (entities) => ({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root1234',
    database: 'prod_db',
    synchronize: false,
    logging: true,
    entities,
  }),
  kakaoSso: {
    clientId: '57918a024c3f67fea70bc5eaf51b51a7',
    clientSecret: 'R50zD42f9PgM8SNUpxcxBWPAhGCnvhsS',
  },
  jwtSecret: 'jwt',
};
