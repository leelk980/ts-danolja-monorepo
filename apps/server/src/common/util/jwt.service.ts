import { environment } from '@danolja/server/common/environment/environment';
import { Injectable } from '@nestjs/common';
import * as JwtManager from 'jsonwebtoken';
import { JsonWebTokenError } from 'jsonwebtoken';

type AuthTokenPayload = {
  type: 'access' | 'refresh';
  id: number;
  iat?: number;
  exp?: number;
};

@Injectable()
export class JwtService {
  private readonly jwtManager = JwtManager;

  issueAuthTokens(payload: { id: number }) {
    const accessTokenPayload: AuthTokenPayload = { type: 'access', id: payload.id };
    const accessToken = this.jwtManager.sign(accessTokenPayload, environment.jwtSecret, {
      expiresIn: '1d',
    });

    const refreshTokenPayload: AuthTokenPayload = { type: 'refresh', id: payload.id };
    const refreshToken = this.jwtManager.sign(refreshTokenPayload, environment.jwtSecret, {
      expiresIn: '30d',
    });

    return { accessToken, refreshToken };
  }

  decodeToken(token: string, tokenType: AuthTokenPayload['type']): AuthTokenPayload {
    const decoded = this.jwtManager.verify(token, environment.jwtSecret) as AuthTokenPayload;
    if (tokenType !== decoded.type) {
      throw new JsonWebTokenError('invalid token');
    }

    return decoded;
  }
}
