import { JwtService } from '@danolja/server/common/util/jwt.service';
import { IS_PUBLIC_KEY } from '@danolja/shared-server';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { ClsService } from 'nestjs-cls';

@Injectable()
export class LoginGuard implements CanActivate {
  public constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
    private readonly clsService: ClsService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) return true;

    const accessToken = req?.headers?.authorization?.split('Bearer ')[1];
    if (!accessToken) return false;

    const payload = await this.jwtService.decodeToken(accessToken, 'access');
    this.clsService.set('userId', payload.id);

    return true;
  }
}
