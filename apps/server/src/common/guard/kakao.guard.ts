import { UserApplicationService } from '@danolja/server/application/user.application-service';
import { environment } from '@danolja/server/common/environment/environment';
import { ApiRouter } from '@danolja/shared';
import { BadRequestException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-kakao';

@Injectable()
export class KakaoGuard extends PassportStrategy(Strategy, 'kakao') {
  constructor(private readonly userApplicationService: UserApplicationService) {
    super({
      clientID: environment.kakaoSso.clientId,
      clientSecret: environment.kakaoSso.clientSecret,
      callbackURL: `${environment.serverUrl}${ApiRouter.ssoKakaoCallback.get.uri}`,
      scope: ['profile', 'account_email'],
    });
  }

  async validate(accessToken: string, refreshToken: string, profile: any, done: any) {
    // eslint-disable-next-line prefer-const
    let { email, username } = profile;
    if (!username) {
      throw new BadRequestException('cannot found email or username');
    }
    if (!email) {
      // TODO: 에러처리
      email = 'leelk98@kakao.com';
    }

    const userId = await this.userApplicationService.singleSignOn({
      email,
      name: username,
      ssoProvider: 'kakao',
      ssoRefreshToken: accessToken,
      ssoAccessToken: refreshToken,
    });

    done(null, { id: userId, email });
  }
}
