import { CoreQueryService } from '@danolja/server/domain/core/service/core.query-service';
import { QuizRoomCommandService } from '@danolja/server/domain/quiz-room/service/quiz-room.command-service';
import { QuizRoomQueryService } from '@danolja/server/domain/quiz-room/service/quiz-room.query-service';
import { QuizQueryService } from '@danolja/server/domain/quiz/service/quiz.query-service';
import { QuizRoomsIdGetView, QuizRoomsIdPatchBody, QuizRoomsPostBody, QuizRoomsPutBody } from '@danolja/shared';
import { BaseApplicationService, Transaction } from '@danolja/shared-server';
import { Injectable } from '@nestjs/common';
import { QuizRoomsIdChatsPostBody } from 'libs/shared/src/router/quiz-room/quiz-rooms-id-chats.dto';

@Injectable()
export class QuizRoomApplicationService extends BaseApplicationService {
  constructor(
    private readonly quizRoomCommandService: QuizRoomCommandService,
    private readonly quizRoomQueryService: QuizRoomQueryService,
    private readonly quizQueryService: QuizQueryService,
    private readonly coreQueryService: CoreQueryService,
  ) {
    super();
  }

  @Transaction()
  async createOne(param: QuizRoomsPostBody): Promise<number> {
    const { quizRoomName, nickname } = param;
    const userId = this.getCurrentUserId();

    return this.quizRoomCommandService.createOne({ name: quizRoomName, user: { id: userId, nickname } });
  }

  async findOneById(param: { id: number }): Promise<QuizRoomsIdGetView> {
    const { id } = param;
    const userId = this.getCurrentUserId();

    await this.quizRoomQueryService.checkUserPermission({ id, userId, role: 'member' });
    const quizRoom = await this.quizRoomQueryService.findOneById(id);

    return {
      name: quizRoom.name,
      status: quizRoom.status,
      accessCode: quizRoom.accessCode,

      quizRoomUsers: quizRoom.quizRoomUsers.map((quizRoomUser) => ({
        id: quizRoomUser.id,
        userId: quizRoomUser.userId,
        nickname: quizRoomUser.nickname,
        isOwner: quizRoomUser.isOwner,
        totalScore: quizRoomUser.totalScore,
      })),

      quizRoomChats: quizRoom.quizRoomChats.map((chat) => ({
        id: chat.id,
        quizRoomUserId: chat.quizRoomUserId,
        message: chat.message,
        isBot: chat.isBot,
        createdAt: chat.createdAt,
      })),

      currentQuiz: await (async () => {
        if (!quizRoom.currentQuiz) {
          return null;
        }

        const currentQuiz = await this.quizQueryService.findOneById(quizRoom.currentQuiz.quizId);
        const categoryMap = await this.coreQueryService.getCategoryMap();

        return {
          id: quizRoom.currentQuiz.id,
          score: quizRoom.currentQuiz.score,
          type: currentQuiz.type,
          title: currentQuiz.title,
          subTitle: currentQuiz.subTitle,
          isManaged: currentQuiz.isManaged,
          quizCategories: currentQuiz.quizCategories.map((each) => {
            const category = categoryMap[each.categoryId];

            return {
              id: category.id,
              name: category.name,
              description: category.description,
            };
          }),
          quizOptions: currentQuiz.quizOptions.map((each) => {
            return {
              id: each.id,
              title: each.title,
              subTitle: each.subTitle,
            };
          }),
        };
      })(),
    };
  }

  @Transaction()
  async updateOne(param: { id: number } & QuizRoomsIdPatchBody): Promise<number> {
    const { id, name, quizRoomQuizzes } = param;
    const userId = this.getCurrentUserId();

    return this.quizRoomCommandService.updateOne({ id, userId, name, quizRoomQuizzes });
  }

  @Transaction()
  async joinById(param: QuizRoomsPutBody): Promise<number> {
    const { nickname, accessCode } = param;
    const userId = this.getCurrentUserId();

    return this.quizRoomCommandService.joinNewUser({ userId, nickname, accessCode });
  }

  @Transaction()
  async createNewChat(param: { quizRoomId: number } & QuizRoomsIdChatsPostBody): Promise<number> {
    const { quizRoomId, message } = param;
    const userId = this.getCurrentUserId();

    const { quizRoomUser } = await this.quizRoomQueryService.checkUserPermission({
      id: quizRoomId,
      userId,
      role: 'member',
    });

    return this.quizRoomCommandService.createNewChat({ quizRoomUserId: quizRoomUser.id, message });
  }
}
