import { CoreQueryService } from '@danolja/server/domain/core/service/core.query-service';
import { QuizQueryService } from '@danolja/server/domain/quiz/service/quiz.query-service';
import { QuizzesGetView, QuizzesIdGetView } from '@danolja/shared';
import { BaseApplicationService } from '@danolja/shared-server';
import { Injectable } from '@nestjs/common';

@Injectable()
export class QuizApplicationService extends BaseApplicationService {
  constructor(
    private readonly quizQueryService: QuizQueryService,
    private readonly coreQueryService: CoreQueryService,
  ) {
    super();
  }

  async findManyByCategoryId(categoryId: number): Promise<QuizzesGetView> {
    await this.coreQueryService.findOneCategoryById(categoryId);
    const quizzes = await this.quizQueryService.findManyByCategoryId(categoryId);

    return {
      quizzes: quizzes.map((quiz) => {
        const { id, type, title, subTitle, isManaged } = quiz;

        return { id, type, title, subTitle, isManaged };
      }),
      pagination: {
        count: quizzes.length,
      },
    };
  }

  async findOneById(id: number): Promise<QuizzesIdGetView> {
    const quiz = await this.quizQueryService.findOneById(id);
    const categoryMap = await this.coreQueryService.getCategoryMap();

    return {
      id,
      type: quiz.type,
      title: quiz.title,
      subTitle: quiz.subTitle,
      isManaged: quiz.isManaged,
      quizCategories: quiz.quizCategories.map((quizCategory) => {
        const { id, categoryId } = quizCategory;
        const { name, description } = categoryMap[categoryId];

        return { id, name, description };
      }),
      quizOptions: quiz.quizOptions.map((quizOption) => {
        const { id, title, subTitle, isCorrect } = quizOption;

        return { id, title, subTitle, isCorrect };
      }),
    };
  }
}
