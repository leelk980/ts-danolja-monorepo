import { CoreQueryService } from '@danolja/server/domain/core/service/core.query-service';
import { CategoriesGetView } from '@danolja/shared';
import { BaseApplicationService } from '@danolja/shared-server';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CoreApplicationService extends BaseApplicationService {
  constructor(private readonly coreQueryService: CoreQueryService) {
    super();
  }

  async findAll(): Promise<CategoriesGetView> {
    const categories = await this.coreQueryService.findAll();

    return {
      categories: categories.map((category) => ({
        id: category.id,
        name: category.name,
        description: category.description,
      })),
      pagination: {
        count: categories.length,
      },
    };
  }
}
