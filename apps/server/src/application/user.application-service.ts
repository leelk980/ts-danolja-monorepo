import { JwtService } from '@danolja/server/common/util/jwt.service';
import { UserCommandService } from '@danolja/server/domain/user/service/user.command-service';
import { UserQueryService } from '@danolja/server/domain/user/service/user.query-service';
import { UserMeGetView } from '@danolja/shared';
import { BaseApplicationService, Transaction } from '@danolja/shared-server';
import { Injectable } from '@nestjs/common';
import { SignInPostBody, SignInPostView } from 'libs/shared/src/router/user/sign-in.dto';

@Injectable()
export class UserApplicationService extends BaseApplicationService {
  constructor(
    private readonly userCommandService: UserCommandService,
    private readonly userQueryService: UserQueryService,
    private readonly jwtService: JwtService,
  ) {
    super();
  }

  @Transaction()
  async singleSignOn(param: {
    name: string;
    email: string;
    ssoProvider: string;
    ssoAccessToken: string;
    ssoRefreshToken: string;
  }): Promise<number> {
    const { name, email, ssoProvider, ssoAccessToken, ssoRefreshToken } = param;

    return this.userCommandService.singleSignOn({
      name,
      email,
      ssoProvider,
      ssoAccessToken,
      ssoRefreshToken,
    });
  }

  @Transaction()
  async signIn(param: SignInPostBody): Promise<SignInPostView> {
    const { email } = param;

    const user = await this.userQueryService.findOneByEmailWithSso(email);
    const tokens = this.jwtService.issueAuthTokens({ id: user.id });

    const { accessToken, refreshToken } = tokens;
    await this.userCommandService.updateOne({ id: user.id, refreshToken });

    return {
      accessToken,
      refreshToken,
    };
  }

  async findCurrentUser(): Promise<UserMeGetView> {
    const userId = this.getCurrentUserId();

    const user = await this.userQueryService.findOneById(userId);

    return {
      id: user.id,
      name: user.name,
      email: user.email,
    };
  }
}
