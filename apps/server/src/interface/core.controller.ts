import { CoreApplicationService } from '@danolja/server/application/core.application-service';
import { ApiRouter, CategoriesGetView } from '@danolja/shared';
import { ApiDefinition } from '@danolja/shared-server';
import { Controller } from '@nestjs/common';

@Controller()
export class CoreController {
  constructor(private readonly coreApplicationService: CoreApplicationService) {}

  @ApiDefinition(ApiRouter.categories.get)
  async findAll(): Promise<CategoriesGetView> {
    return this.coreApplicationService.findAll();
  }
}
