import { QuizRoomApplicationService } from '@danolja/server/application/quiz-room.application-service';
import {
  ApiRouter,
  QuizRoomsIdChatsPostBody,
  QuizRoomsIdGetView,
  QuizRoomsIdPatchBody,
  QuizRoomsPostBody,
  QuizRoomsPutBody,
} from '@danolja/shared';
import { ApiDefinition } from '@danolja/shared-server';
import { Body, Controller, Param } from '@nestjs/common';

@Controller()
export class QuizRoomController {
  constructor(private readonly quizRoomApplicationService: QuizRoomApplicationService) {}

  @ApiDefinition(ApiRouter.quizRooms.post)
  async createOne(@Body() body: QuizRoomsPostBody): Promise<number> {
    return this.quizRoomApplicationService.createOne(body);
  }

  @ApiDefinition(ApiRouter.quizRooms.put)
  async joinById(@Body() body: QuizRoomsPutBody): Promise<number> {
    return this.quizRoomApplicationService.joinById(body);
  }

  @ApiDefinition(ApiRouter.quizRoomsId.get)
  async findOneById(@Param('id') id: number): Promise<QuizRoomsIdGetView> {
    return this.quizRoomApplicationService.findOneById({ id });
  }

  @ApiDefinition(ApiRouter.quizRoomsId.patch)
  async updateOne(@Param('id') id: number, @Body() body: QuizRoomsIdPatchBody): Promise<number> {
    return this.quizRoomApplicationService.updateOne({ id, ...body });
  }

  @ApiDefinition(ApiRouter.quizRoomsIdChats.post)
  async createNewChat(@Param('id') id: number, @Body() body: QuizRoomsIdChatsPostBody): Promise<number> {
    return this.quizRoomApplicationService.createNewChat({ quizRoomId: id, ...body });
  }
}
