import { UserApplicationService } from '@danolja/server/application/user.application-service';
import { environment } from '@danolja/server/common/environment/environment';
import { ApiRouter, SignInPostBody, SignInPostView, UserMeGetView } from '@danolja/shared';
import { ApiDefinition } from '@danolja/shared-server';
import { Body, Controller, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request, Response } from 'express';

@Controller()
export class UserController {
  constructor(private readonly userApplicationService: UserApplicationService) {}

  @UseGuards(AuthGuard('kakao'))
  @ApiDefinition(ApiRouter.ssoKakao.get)
  // eslint-disable-next-line
  async ssoKakao() {}

  @UseGuards(AuthGuard('kakao'))
  @ApiDefinition(ApiRouter.ssoKakaoCallback.get)
  async ssoKakaoCallback(@Req() req: Request, @Res() res: Response) {
    const email = (req.user as { email: string }).email;
    return res.redirect(`${environment.webUrl}/_sso?provider=kakao&email=${email}`);
  }

  @ApiDefinition(ApiRouter.signIn.post)
  async signIn(@Body() body: SignInPostBody): Promise<SignInPostView> {
    return this.userApplicationService.signIn(body);
  }

  @ApiDefinition(ApiRouter.userMe.get)
  async findUserMe(): Promise<UserMeGetView> {
    return this.userApplicationService.findCurrentUser();
  }
}
