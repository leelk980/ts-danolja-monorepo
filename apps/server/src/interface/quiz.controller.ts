import { QuizApplicationService } from '@danolja/server/application/quiz.application-service';
import { ApiRouter, QuizzesGetQuery, QuizzesGetView, QuizzesIdGetView } from '@danolja/shared';
import { ApiDefinition } from '@danolja/shared-server';
import { Controller, Param, Query } from '@nestjs/common';

@Controller()
export class QuizController {
  constructor(private readonly quizApplicationService: QuizApplicationService) {}

  @ApiDefinition(ApiRouter.quizzes.get)
  async findMany(@Query() query: QuizzesGetQuery): Promise<QuizzesGetView> {
    return this.quizApplicationService.findManyByCategoryId(query.categoryId);
  }

  @ApiDefinition(ApiRouter.quizzesId.get)
  async findOne(@Param('id') id: number): Promise<QuizzesIdGetView> {
    return this.quizApplicationService.findOneById(id);
  }
}
