import { environment } from '@danolja/server/common/environment/environment';
import { BaseSocketEvent, SocketEvent } from '@danolja/shared';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

// namespace: /\/ws-.+/,
@WebSocketGateway({ allowEIO3: true, cors: { origin: environment.webUrl } })
export class WsGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  private readonly io!: Server;

  @SubscribeMessage(SocketEvent.quizRoomUpdated.name)
  handleMessage(@ConnectedSocket() socket: Socket, @MessageBody() payload: typeof SocketEvent.quizRoomUpdated.payload) {
    console.log(payload);
    const { id, type } = payload;
    const roomName = `quiz-room:${id}`;

    if (type === 'join') {
      socket.join(roomName);
      this.emitToRoom({ roomName, event: SocketEvent.quizRoomUpdated, payload: { id, type } });
    }

    if (type === 'start') {
      this.emitToRoom({ roomName, event: SocketEvent.quizRoomUpdated, payload: { id, type } });
    }

    if (type === 'refetch') {
      this.emitToRoom({ roomName, event: SocketEvent.quizRoomUpdated, payload: { id, type } });
    }
  }

  afterInit() {
    //
  }

  handleConnection(@ConnectedSocket() socket: Socket) {
    console.log('connected:', socket.id);
  }

  handleDisconnect(@ConnectedSocket() socket: Socket) {
    console.log('disconnected:', socket.id);
  }

  private emitToRoom<T extends BaseSocketEvent<unknown>>(param: { event: T; payload: T['payload']; roomName: string }) {
    const { event, payload, roomName } = param;

    this.io.in(roomName).emit(event.name, payload);
  }
}
