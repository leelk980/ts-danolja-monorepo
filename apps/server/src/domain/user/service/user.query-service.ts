import { UserRepository } from '@danolja/server/domain/user/repository/user.repository';
import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class UserQueryService {
  constructor(private readonly userRepository: UserRepository) {}

  async findOneById(id: number) {
    return await this.userRepository.findOneById(id);
  }

  async findOneByEmailWithSso(email: string) {
    const user = await this.userRepository.findOneByEmailWithSso(email);
    if (!user.userSso.id) {
      throw new NotFoundException(`cannot found user by email: ${email}`);
    }

    return user;
  }
}
