import { UserSsoRepository } from '@danolja/server/domain/user/repository/user-sso.repository';
import { UserRepository } from '@danolja/server/domain/user/repository/user.repository';
import { UserSsoProviderEnum } from '@danolja/shared-server';
import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export class UserCommandService {
  constructor(private readonly userRepository: UserRepository, private readonly userSsoRepository: UserSsoRepository) {}

  async singleSignOn(param: {
    name: string;
    email: string;
    ssoProvider: string;
    ssoAccessToken: string;
    ssoRefreshToken: string;
  }): Promise<number> {
    const { name, email, ssoProvider, ssoAccessToken, ssoRefreshToken } = param;
    if (!['google', 'kakao'].includes(ssoProvider)) {
      throw new BadRequestException(`invalid sso provider: ${ssoProvider}`);
    }

    const user = await this.userRepository.findOneByEmailWithSso(email);

    // signUp
    if (!user) {
      const userId = await this.userRepository.saveOne(
        await this.userRepository.createOne({
          name,
          email,
          refreshToken: null,
        }),
      );

      await this.userSsoRepository.saveOne(
        this.userSsoRepository.createOne({
          userId,
          ssoProvider: ssoProvider as UserSsoProviderEnum,
          ssoRefreshToken,
          ssoAccessToken,
        }),
      );

      return userId;
    }

    // signIn
    await this.userSsoRepository.updateOne({
      ...user.userSso,
      ssoAccessToken,
      ssoRefreshToken,
    });

    return user.id;
  }

  async updateOne(param: { id: number; refreshToken?: string }): Promise<number> {
    const { id, refreshToken } = param;
    const user = await this.userRepository.findOneById(id);

    if (refreshToken) {
      user.refreshToken = refreshToken;
    }

    await this.userRepository.saveOne(user);

    return id;
  }
}
