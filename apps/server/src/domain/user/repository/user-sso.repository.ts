import { UserSsoTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/user/user-sso.typeorm-repository';
import { BaseRepository, Repository, UserSsoEntity } from '@danolja/shared-server';

@Repository(UserSsoEntity, UserSsoTypeormRepository)
export abstract class UserSsoRepository extends BaseRepository<UserSsoEntity, 'user'> {}
