import { UserTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/user/user.typeorm-repository';
import { BaseRepository, JoinEntity, Repository, UserEntity } from '@danolja/shared-server';

@Repository(UserEntity, UserTypeormRepository)
export abstract class UserRepository extends BaseRepository<UserEntity, 'userSso'> {
  abstract findOneByEmailWithSso(email: string): Promise<JoinEntity<UserEntity, 'userSso'>>;
}
