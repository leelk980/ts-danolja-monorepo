import { QuizRoomRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room.repository';
import { ForbiddenException, Injectable } from '@nestjs/common';

@Injectable()
export class QuizRoomQueryService {
  constructor(private readonly quizRoomRepository: QuizRoomRepository) {}

  async checkUserPermission(params: { id: number; userId: number; role: 'member' | 'owner' }) {
    const { id, userId, role } = params;

    const quizRoom = await this.quizRoomRepository.findOneByIdWithUsers(id);

    const quizRoomUser = quizRoom.quizRoomUsers.find((each) => each.userId === userId);
    if (!quizRoomUser) {
      throw new ForbiddenException(`forbidden quizRoom: ${id}, user: ${userId} - not member`);
    }
    if (role === 'owner' && !quizRoomUser.isOwner) {
      throw new ForbiddenException(`forbidden quizRoom: ${id}, user: ${userId} - not owner`);
    }

    return { quizRoom, quizRoomUser };
  }

  async findOneById(id: number) {
    const quizRoom = await this.quizRoomRepository.findOneByIdWithQuizUserAnswerChats(id);

    return {
      ...quizRoom,

      quizRoomUsers: quizRoom.quizRoomUsers.map((quizRoomUser) => {
        const { quizRoomAnswers } = quizRoomUser;
        const quizRoomQuizMap = quizRoom.quizRoomQuizzes.associateBy('id');

        const totalScore = quizRoomAnswers.reduce((acc, each) => {
          if (!each.isCorrect) {
            return acc;
          }

          return acc + quizRoomQuizMap[each.quizRoomQuizId].score;
        }, 0);

        return { ...quizRoomUser, totalScore };
      }),

      quizRoomChats: quizRoom.quizRoomUsers
        .map((quizRoomUser) => quizRoomUser.quizRoomChats)
        .flat()
        .sortBy('id', 'asc'),

      currentQuiz: (() => {
        const lastAnsweredQuizRoomQuizId = quizRoom.quizRoomUsers
          ?.find((quizRoomUser) => !quizRoomUser.isOwner)
          ?.quizRoomAnswers.map((each) => each.quizRoomQuizId)
          ?.pop();

        const lastAnsweredQuizRoomQuizIndex = quizRoom.quizRoomQuizzes.findIndex(
          (each) => each.id === lastAnsweredQuizRoomQuizId,
        );
        if (lastAnsweredQuizRoomQuizIndex === -1 && quizRoom.status === 'ready') {
          return null;
        }

        return quizRoom.quizRoomQuizzes[lastAnsweredQuizRoomQuizIndex + 1];
      })(),
    };
  }
}
