import { QuizRoomAnswerRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-answer.repository';
import { QuizRoomChatRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-chat.repository';
import { QuizRoomQuizRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-quiz.repository';
import { QuizRoomUserRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room-user.repository';
import { QuizRoomRepository } from '@danolja/server/domain/quiz-room/repository/quiz-room.repository';
import { BadRequestException, ConflictException, Injectable } from '@nestjs/common';

@Injectable()
export class QuizRoomCommandService {
  constructor(
    private readonly quizRoomRepository: QuizRoomRepository,
    private readonly quizRoomChatRepository: QuizRoomChatRepository,
    private readonly quizRoomUserRepository: QuizRoomUserRepository,
    private readonly quizRoomQuizRepository: QuizRoomQuizRepository,
    private readonly quizRoomAnswerRepository: QuizRoomAnswerRepository,
  ) {}

  async createOne(param: { name: string; user: { id: number; nickname: string } }) {
    const { name, user } = param;

    const quizRoomId = await this.quizRoomRepository.saveOne(
      this.quizRoomRepository.createOne({
        name,
        status: 'ready',
        accessCode: `${name.slice(0, 4)}12`,
      }),
    );

    const quizRoomUserId = await this.quizRoomUserRepository.saveOne(
      this.quizRoomUserRepository.createOne({
        quizRoomId,
        userId: user.id,
        nickname: user.nickname,
        isOwner: true,
      }),
    );

    await this.quizRoomChatRepository.saveOne(
      this.quizRoomChatRepository.createOne({
        quizRoomUserId,
        message: `${user.nickname}님이 입장하셨습니다.`,
        isBot: true,
      }),
    );

    return quizRoomId;
  }

  async updateOne(param: {
    id: number;
    userId: number;
    name?: string;
    quizRoomQuizzes?: { id: number; score: number }[];
  }) {
    const { id, userId, name, quizRoomQuizzes } = param;

    const quizRoom = await this.quizRoomRepository.findOneByIdWithUsersAndQuizzes(id);
    if (quizRoom.status !== 'ready') {
      throw new BadRequestException(`invalid quiz room: ${id}, status: ${quizRoom.status}`);
    }

    const quizRoomOwner = quizRoom.quizRoomUsers.find((quizRoomUser) => quizRoomUser.isOwner);
    if (!quizRoomOwner || quizRoomOwner.userId !== userId) {
      throw new BadRequestException(`not permitted to quizRoom: ${id}, userId: ${userId}`);
    }

    if (quizRoomQuizzes) {
      await this.quizRoomQuizRepository.deleteMany(quizRoom.quizRoomQuizzes);
      await this.quizRoomQuizRepository.saveMany(
        this.quizRoomQuizRepository.createMany(
          quizRoomQuizzes.map((quiz) => ({
            quizRoomId: id,
            quizId: quiz.id,
            score: quiz.score,
          })),
        ),
      );
    }

    if (name || quizRoomQuizzes) {
      await this.quizRoomRepository.updateOne({
        ...quizRoom,
        name: name ?? quizRoom.name,
        status: 'proceeding',
      });
    }

    return id;
  }

  async joinNewUser(param: { userId: number; nickname: string; accessCode: string }) {
    const { userId, nickname, accessCode } = param;

    const quizRoom = await this.quizRoomRepository.findOneByAccessCodeWithUsers(accessCode);
    if (quizRoom.quizRoomUsers.find((each) => each.userId === userId)) {
      return quizRoom.id;
    }

    if (quizRoom.quizRoomUsers.find((each) => each.nickname === nickname)) {
      throw new ConflictException(`conflict nickname: ${nickname}`);
    }

    const quizRoomUserId = await this.quizRoomUserRepository.saveOne(
      this.quizRoomUserRepository.createOne({
        quizRoomId: quizRoom.id,
        userId,
        nickname,
        isOwner: false,
      }),
    );

    await this.quizRoomChatRepository.saveOne(
      this.quizRoomChatRepository.createOne({
        quizRoomUserId,
        message: `${nickname}님이 입장하셨습니다.`,
        isBot: true,
      }),
    );

    return quizRoom.id;
  }

  async createNewChat(param: { quizRoomUserId: number; message: string }) {
    const { quizRoomUserId, message } = param;

    return this.quizRoomChatRepository.saveOne(
      this.quizRoomChatRepository.createOne({
        quizRoomUserId,
        message,
        isBot: false,
      }),
    );
  }
}
