import { QuizRoomAnswerTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz-room/quiz-room-answer.typeorm-repository';
import { BaseRepository, QuizRoomAnswerEntity, Repository } from '@danolja/shared-server';

@Repository(QuizRoomAnswerEntity, QuizRoomAnswerTypeormRepository)
export abstract class QuizRoomAnswerRepository extends BaseRepository<QuizRoomAnswerEntity> {}
