import { QuizRoomUserTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz-room/quiz-room-user.typeorm-repository';
import { BaseRepository, QuizRoomUserEntity, Repository } from '@danolja/shared-server';

@Repository(QuizRoomUserEntity, QuizRoomUserTypeormRepository)
export abstract class QuizRoomUserRepository extends BaseRepository<QuizRoomUserEntity, 'quizRoom' | 'quizRoomChats'> {}
