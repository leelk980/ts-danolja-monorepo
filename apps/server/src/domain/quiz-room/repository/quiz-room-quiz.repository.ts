import { QuizRoomQuizTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz-room/quiz-room-quiz.typeorm-repository';
import { BaseRepository, QuizRoomQuizEntity, Repository } from '@danolja/shared-server';

@Repository(QuizRoomQuizEntity, QuizRoomQuizTypeormRepository)
export abstract class QuizRoomQuizRepository extends BaseRepository<QuizRoomQuizEntity, 'quizRoom'> {}
