import { QuizRoomTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz-room/quiz-room.typeorm-repository';
import { BaseRepository, JoinEntity, QuizRoomEntity, QuizRoomUserEntity, Repository } from '@danolja/shared-server';

@Repository(QuizRoomEntity, QuizRoomTypeormRepository)
export abstract class QuizRoomRepository extends BaseRepository<QuizRoomEntity, 'quizRoomQuizzes' | 'quizRoomUsers'> {
  abstract findOneByIdWithUsers(id: number): Promise<JoinEntity<QuizRoomEntity, 'quizRoomUsers', 'quizRoomQuizzes'>>;

  abstract findOneByAccessCodeWithUsers(
    accessCode: string,
  ): Promise<JoinEntity<QuizRoomEntity, 'quizRoomUsers', 'quizRoomQuizzes'>>;

  abstract findOneByIdWithUsersAndQuizzes(
    id: number,
  ): Promise<JoinEntity<QuizRoomEntity, 'quizRoomUsers' | 'quizRoomQuizzes'>>;

  abstract findOneByIdWithQuizUserAnswerChats(id: number): Promise<
    JoinEntity<QuizRoomEntity, 'quizRoomQuizzes', 'quizRoomUsers'> & {
      quizRoomUsers: JoinEntity<QuizRoomUserEntity, 'quizRoomAnswers' | 'quizRoomChats', 'quizRoom'>[];
    }
  >;
}
