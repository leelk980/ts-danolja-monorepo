import { QuizRoomChatTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz-room/quiz-room-chat.typeorm-repository';
import { BaseRepository, QuizRoomChatEntity, Repository } from '@danolja/shared-server';

@Repository(QuizRoomChatEntity, QuizRoomChatTypeormRepository)
export abstract class QuizRoomChatRepository extends BaseRepository<QuizRoomChatEntity, 'quizRoomUser'> {}
