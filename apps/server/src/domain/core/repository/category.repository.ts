import { CategoryTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/core/category.typeorm-repository';
import { BaseRepository, CategoryEntity, Repository } from '@danolja/shared-server';

@Repository(CategoryEntity, CategoryTypeormRepository)
export abstract class CategoryRepository extends BaseRepository<CategoryEntity> {}
