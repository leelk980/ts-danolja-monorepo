import { CategoryRepository } from '@danolja/server/domain/core/repository/category.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CoreCommandService {
  constructor(private readonly categoryRepository: CategoryRepository) {}

  async createOne() {}
}
