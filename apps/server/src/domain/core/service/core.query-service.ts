import { CategoryRepository } from '@danolja/server/domain/core/repository/category.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CoreQueryService {
  constructor(private readonly categoryRepository: CategoryRepository) {}

  async findAll() {
    return this.categoryRepository.findAll();
  }

  async findOneCategoryById(categoryId: number) {
    return this.categoryRepository.findOneById(categoryId);
  }

  async getCategoryMap() {
    return (await this.categoryRepository.findAll()).associateBy('id');
  }
}
