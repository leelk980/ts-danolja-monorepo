import { QuizCategoryRepository } from '@danolja/server/domain/quiz/repository/quiz-category.repository';
import { QuizOptionRepository } from '@danolja/server/domain/quiz/repository/quiz-option.repository';
import { QuizRepository } from '@danolja/server/domain/quiz/repository/quiz.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class QuizCommandService {
  constructor(
    private readonly quizRepository: QuizRepository,
    private readonly quizCategoryRepository: QuizCategoryRepository,
    private readonly quizOptionRepository: QuizOptionRepository,
  ) {}

  async createOne() {
    const newQuizId = await this.quizRepository.saveOne(
      this.quizRepository.createOne({
        title: 'test',
        subTitle: 'test2',
        type: 'radio',
        isManaged: true,
        parentId: null,
      }),
    );

    await this.quizCategoryRepository.saveMany(
      this.quizCategoryRepository.createMany([
        {
          quizId: newQuizId,
          categoryId: 1,
        },
      ]),
    );

    await this.quizOptionRepository.saveMany(
      this.quizOptionRepository.createMany([
        {
          quizId: newQuizId,
          title: '123',
          subTitle: '123',
          isCorrect: null,
        },
      ]),
    );
  }
}
