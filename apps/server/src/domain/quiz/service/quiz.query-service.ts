import { QuizRepository } from '@danolja/server/domain/quiz/repository/quiz.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class QuizQueryService {
  constructor(private readonly quizRepository: QuizRepository) {}

  async findManyByCategoryId(categoryId: number) {
    return this.quizRepository.findManyByCategoryId(categoryId);
  }

  async findOneById(id: number) {
    return await this.quizRepository.findOneByIdWithJoin(id);
  }
}
