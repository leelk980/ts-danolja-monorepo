import { QuizCategoryTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz/quiz-category.typeorm-repository';
import { BaseRepository, QuizCategoryEntity, Repository } from '@danolja/shared-server';

@Repository(QuizCategoryEntity, QuizCategoryTypeormRepository)
export abstract class QuizCategoryRepository extends BaseRepository<QuizCategoryEntity, 'quiz'> {}
