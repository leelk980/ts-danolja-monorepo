import { QuizTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz/quiz.typeorm-repository';
import { BaseRepository, JoinEntity, QuizEntity, Repository } from '@danolja/shared-server';

@Repository(QuizEntity, QuizTypeormRepository)
export abstract class QuizRepository extends BaseRepository<QuizEntity, 'quizOptions' | 'quizCategories'> {
  abstract findManyByCategoryId(
    categoryId: number,
  ): Promise<JoinEntity<QuizEntity, never, 'quizOptions' | 'quizCategories'>[]>;

  abstract findOneByIdWithJoin(id: number): Promise<JoinEntity<QuizEntity, 'quizOptions' | 'quizCategories'>>;
}
