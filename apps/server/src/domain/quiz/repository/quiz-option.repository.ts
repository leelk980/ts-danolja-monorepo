import { QuizOptionTypeormRepository } from '@danolja/server/infrastructure/typeorm-repository/quiz/quiz-option.typeorm-repository';
import { BaseRepository, QuizOptionEntity, Repository } from '@danolja/shared-server';

@Repository(QuizOptionEntity, QuizOptionTypeormRepository)
export abstract class QuizOptionRepository extends BaseRepository<QuizOptionEntity, 'quiz'> {}
