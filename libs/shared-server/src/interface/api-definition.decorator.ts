import { ServerApi } from '@danolja/shared';
import { applyDecorators, Delete, Get, Patch, Post, Put } from '@nestjs/common';
import { InvalidClassException } from '@nestjs/core/errors/exceptions/invalid-class.exception';
import { Public } from 'libs/shared-server/src/interface/public.decorator';

/* eslint-disable  */
export const ApiDefinition = (serverApi: ServerApi) => {
  const { uri, method, auth } = serverApi;
  const decorators: any[] = [auth === 'public' ? Public() : () => {}];

  if (method === 'get') {
    decorators.push(Get(uri));
  } else if (method === 'post') {
    decorators.push(Post(uri));
  } else if (method === 'put') {
    decorators.push(Put(uri));
  } else if (method === 'patch') {
    decorators.push(Patch(uri));
  } else if (method === 'delete') {
    decorators.push(Delete(uri));
  } else {
    throw new InvalidClassException(`invalid server api router ${serverApi}`);
  }

  return applyDecorators(...decorators);
};
