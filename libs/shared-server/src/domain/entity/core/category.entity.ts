import { BaseDateEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { Column, Entity } from 'typeorm';

// TreeRepository
@Entity('category')
export class CategoryEntity extends BaseDateEntity {
  @Column('varchar', { name: 'name' })
  name!: string;

  @Column('varchar', { name: 'description' })
  description!: string;
}
