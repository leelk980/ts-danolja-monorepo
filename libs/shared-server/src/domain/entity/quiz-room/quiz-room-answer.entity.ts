import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizRoomUserEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room-user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity('quiz_room_answer')
export class QuizRoomAnswerEntity extends BaseEntity {
  @Column('int', { name: 'quiz_room_quiz_id' })
  quizRoomQuizId!: number;

  @Column('int', { name: 'quiz_room_user_id' })
  quizRoomUserId!: number;

  @Column('varchar', { name: 'input' })
  input!: string;

  @Column('boolean', { name: 'is_correct' })
  isCorrect!: boolean;

  /** Relations */
  // @ManyToOne(() => QuizRoomQuizEntity, (qrq) => qrq.)
  // @JoinColumn({ name: 'quiz_room_quiz_id' })
  // quizRoomQuiz?: QuizRoomQuizEntity;

  @ManyToOne(() => QuizRoomUserEntity, (qru) => qru.quizRoomAnswers)
  @JoinColumn({ name: 'quiz_room_user_id' })
  quizRoomUser?: QuizRoomUserEntity;
}
