import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizRoomUserEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room-user.entity';
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity('quiz_room_chat')
export class QuizRoomChatEntity extends BaseEntity {
  @Column('int', { name: 'quiz_room_user_id' })
  quizRoomUserId!: number;

  @Column('varchar', { name: 'message' })
  message!: string;

  @Column('boolean', { name: 'is_bot' })
  isBot!: boolean;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  createdAt!: Date;

  /** Relations */
  @ManyToOne(() => QuizRoomUserEntity, (qru) => qru.quizRoomChats)
  @JoinColumn({ name: 'quiz_room_user_id' })
  quizRoomUser?: QuizRoomUserEntity;
}
