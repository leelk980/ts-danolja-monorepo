import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizRoomAnswerEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room-answer.entity';
import { QuizRoomChatEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room-chat.entity';
import { QuizRoomEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

@Entity('quiz_room_user')
export class QuizRoomUserEntity extends BaseEntity {
  @Column('int', { name: 'quiz_room_id' })
  quizRoomId!: number;

  @Column('int', { name: 'user_id' })
  userId!: number;

  @Column('varchar', { name: 'nickname' })
  nickname!: string;

  @Column('boolean', { name: 'is_owner' })
  isOwner!: boolean;

  /** Relations */
  @ManyToOne(() => QuizRoomEntity, (qr) => qr.quizRoomUsers)
  @JoinColumn({ name: 'quiz_room_id' })
  quizRoom?: QuizRoomEntity;

  @OneToMany(() => QuizRoomChatEntity, (qrc) => qrc.quizRoomUser)
  quizRoomChats?: QuizRoomChatEntity[];

  @OneToMany(() => QuizRoomAnswerEntity, (qra) => qra.quizRoomUser)
  quizRoomAnswers?: QuizRoomAnswerEntity[];
}
