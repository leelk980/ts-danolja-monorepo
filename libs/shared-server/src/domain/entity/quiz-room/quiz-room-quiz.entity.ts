import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizRoomEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity('quiz_room_quiz')
export class QuizRoomQuizEntity extends BaseEntity {
  @Column('int', { name: 'quiz_room_id' })
  quizRoomId!: number;

  @Column('int', { name: 'quiz_id' })
  quizId!: number;

  @Column('int', { name: 'score' })
  score!: number;

  /** Relations */
  @ManyToOne(() => QuizRoomEntity, (qr) => qr.quizRoomQuizzes)
  @JoinColumn({ name: 'quiz_room_id' })
  quizRoom?: QuizRoomEntity;
}
