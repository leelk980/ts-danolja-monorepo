import { BaseDateEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizRoomQuizEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room-quiz.entity';
import { QuizRoomUserEntity } from 'libs/shared-server/src/domain/entity/quiz-room/quiz-room-user.entity';
import { Column, Entity, OneToMany } from 'typeorm';

export type QuizRoomStatusEnum = 'ready' | 'proceeding' | 'finish';

@Entity('quiz_room')
export class QuizRoomEntity extends BaseDateEntity {
  @Column('varchar', { name: 'name' })
  name!: string;

  @Column('varchar', { name: 'status' })
  status!: QuizRoomStatusEnum;

  @Column('varchar', { name: 'access_code' })
  accessCode!: string;

  /** Relations */
  @OneToMany(() => QuizRoomQuizEntity, (qrq) => qrq.quizRoom)
  quizRoomQuizzes?: QuizRoomQuizEntity[];

  @OneToMany(() => QuizRoomUserEntity, (qru) => qru.quizRoom)
  quizRoomUsers?: QuizRoomUserEntity[];
}
