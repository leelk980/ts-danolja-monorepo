import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { UserEntity } from 'libs/shared-server/src/domain/entity/user/user.entity';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

export type UserSsoProviderEnum = 'kakao' | 'google';

@Entity('user_sso')
export class UserSsoEntity extends BaseEntity {
  @Column('varchar', { name: 'sso_provider' })
  ssoProvider!: UserSsoProviderEnum;

  @Column('varchar', { name: 'sso_access_token' })
  ssoAccessToken!: string;

  @Column('varchar', { name: 'sso_refresh_token' })
  ssoRefreshToken!: string;

  @Column('int', { name: 'user_id' })
  userId!: number;

  /** Relations */
  @OneToOne(() => UserEntity, (u) => u.userSso)
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity;
}
