import { BaseDateEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { UserSsoEntity } from 'libs/shared-server/src/domain/entity/user/user-sso.entity';
import { Column, Entity, OneToOne } from 'typeorm';

@Entity('user')
export class UserEntity extends BaseDateEntity {
  @Column('varchar', { name: 'name' })
  name!: string;

  @Column('varchar', { name: 'email', unique: true })
  email!: string;

  @Column('varchar', { name: 'refresh_token', nullable: true })
  refreshToken!: string | null;

  /** Relations */
  @OneToOne(() => UserSsoEntity, (us) => us.user)
  userSso?: UserSsoEntity;
}
