import { CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export abstract class BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id!: number;
}

export abstract class BaseDateEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id!: number;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  updatedAt!: Date;

  @DeleteDateColumn({ type: 'timestamp', name: 'deleted_at' })
  deletedAt!: Date;
}

/**
 * @description
 * Create Entity 할때 사용할 유틸리티 타입
 * - T = target entity.
 * - U = entity에서 omit 시킬 property. ex) relation property
 * - V = entity에서 optional 시킬 property. ex) default value가 있는 property
 *
 * => T로 넣은 entity에 base property + U가 omit 되고 V가 partial 된 타입.
 * */
export type CreateEntity<
  T extends BaseEntity,
  U extends keyof Omit<T, keyof BaseDateEntity> = never,
  V extends keyof Omit<T, keyof BaseDateEntity> = never,
> = Omit<T, keyof BaseDateEntity | U | V> & Partial<{ [key in V]: T[key] }>;

/**
 * @description
 * Join 걸고 Find Entity 할때 사용할 유틸리티 타입
 * - T = target entity.
 * - U = required 시킬 property. ex) relation property
 * - V = omit 시킬 property
 *
 * => entity는 join property가 undefined일 수 있게 되어있기 때문에 required로 바꿔주는 용도
 * */
export type JoinEntity<
  T extends BaseEntity,
  U extends keyof Omit<T, keyof BaseDateEntity> = never,
  V extends keyof Omit<T, keyof BaseDateEntity> = never,
> = Omit<T, U | V> & Required<Pick<T, U>>;
