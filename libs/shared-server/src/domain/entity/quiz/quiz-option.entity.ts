import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizEntity } from 'libs/shared-server/src/domain/entity/quiz/quiz.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity('quiz_option')
export class QuizOptionEntity extends BaseEntity {
  @Column('text', { name: 'title' })
  title!: string;

  @Column('text', { name: 'sub_title' })
  subTitle!: string;

  @Column('int', { name: 'quiz_id' })
  quizId!: number;

  @Column('boolean', { name: 'is_correct', nullable: true })
  isCorrect!: boolean | null;

  /** Relations */
  @ManyToOne(() => QuizEntity, (q) => q.quizOptions)
  @JoinColumn({ name: 'quiz_id' })
  quiz?: QuizEntity;
}
