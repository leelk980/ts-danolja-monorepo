import { BaseDateEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizCategoryEntity } from 'libs/shared-server/src/domain/entity/quiz/quiz-category.entity';
import { QuizOptionEntity } from 'libs/shared-server/src/domain/entity/quiz/quiz-option.entity';
import { Column, Entity, OneToMany } from 'typeorm';

export type QuizTypeEnum = 'radio' | 'checkbox' | 'input';

@Entity('quiz')
export class QuizEntity extends BaseDateEntity {
  @Column('varchar', { name: 'type' })
  type!: QuizTypeEnum; // enum

  @Column('varchar', { name: 'title' })
  title!: string;

  @Column('varchar', { name: 'sub_title' })
  subTitle!: string;

  @Column('boolean', { name: 'is_managed' })
  isManaged!: boolean;

  @Column('int', { name: 'parent_id', nullable: true })
  parentId!: number | null;

  /** Relations */
  @OneToMany(() => QuizCategoryEntity, (qc) => qc.quiz)
  quizCategories?: QuizCategoryEntity[];

  @OneToMany(() => QuizOptionEntity, (qo) => qo.quiz)
  quizOptions?: QuizOptionEntity[];
}
