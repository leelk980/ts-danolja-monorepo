import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { QuizEntity } from 'libs/shared-server/src/domain/entity/quiz/quiz.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity('quiz_category')
export class QuizCategoryEntity extends BaseEntity {
  @Column('int', { name: 'category_id' })
  categoryId!: number;

  @Column('int', { name: 'quiz_id' })
  quizId!: number;

  /** Relations */
  @ManyToOne(() => QuizEntity, (q) => q.quizOptions)
  @JoinColumn({ name: 'quiz_id' })
  quiz?: QuizEntity;
}
