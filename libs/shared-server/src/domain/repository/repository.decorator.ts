/* eslint-disable @typescript-eslint/no-explicit-any */
import { SetMetadata } from '@nestjs/common';
import { BaseEntity } from 'libs/shared-server/src/domain/entity/base.entity';
import { TypeormRepository } from 'libs/shared-server/src/infrastructure';

export const REPOSITORY_DECORATOR = 'REPOSITORY_DECORATOR';

/**
 * @Description
 * - typeorm 0.3.0 버전부터 customRepository 기능이 deprecated 되어서 직접 구현함
 * - repository에 entity와 구현체를 명시해야함
 * */

export function Repository<T extends BaseEntity>(
  entity: new (...args: any[]) => T,
  implement: typeof TypeormRepository<T>, // | PrismaRepository<T>
): ClassDecorator {
  return SetMetadata(REPOSITORY_DECORATOR, [entity, implement]);
}
