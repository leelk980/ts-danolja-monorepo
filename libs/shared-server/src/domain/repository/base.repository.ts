import { BaseDateEntity, BaseEntity, CreateEntity, JoinEntity } from 'libs/shared-server/src/domain/entity/base.entity';

/**
 * @description
 * T = Entity
 * K = Relation properties
 * */
export abstract class BaseRepository<T extends BaseEntity, K extends keyof Omit<T, keyof BaseDateEntity> = never> {
  abstract createOne(entity: CreateEntity<T, K>): T;

  abstract createMany(entities: CreateEntity<T, K>[]): T[];

  abstract saveOne(entity: T): Promise<number>;

  abstract saveMany(entities: T[]): Promise<number[]>;

  abstract updateOne(entity: T): Promise<number>;

  // TODO: updateBatch()

  abstract deleteOne(entity: T): Promise<number>;

  abstract deleteMany(entities: T[]): Promise<number[]>;

  abstract deleteAll(): Promise<void>;

  /** Join 걸지 않으므로 K를 Omit */
  abstract findAll(): Promise<JoinEntity<T, never, K>[]>;

  abstract findOneByIdOrNull(id: number): Promise<JoinEntity<T, never, K> | null>;

  abstract findOneById(id: number): Promise<JoinEntity<T, never, K>>;

  abstract findManyByIds(ids: number[]): Promise<JoinEntity<T, never, K>[]>;
}
