import { environment } from '@danolja/server/common/environment/environment';
import { INestApplication, InternalServerErrorException } from '@nestjs/common';

let _nestApplication: INestApplication;

export const initApplication = async (options: { application: INestApplication; port: number }) => {
  const { application, port } = options;

  _nestApplication = application;

  application.setGlobalPrefix('api');
  application.enableCors({ origin: environment.webUrl });

  await application.listen(port);
};

export const getApplication = () => {
  if (!_nestApplication) {
    throw new InternalServerErrorException('not found nest app');
  }

  return _nestApplication;
};
