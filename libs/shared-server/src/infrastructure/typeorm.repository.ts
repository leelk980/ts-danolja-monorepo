import { NotFoundException, OnModuleInit } from '@nestjs/common';
import { getApplication } from 'libs/shared-server/src/common';
import { BaseEntity, BaseRepository } from 'libs/shared-server/src/domain';
import { ClsService } from 'nestjs-cls';
import { QueryRunner, Repository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

/** @description
 *  RepositoryImpl에서 상속받을 abstract class
 *  - 트랜잭션을 위한 queryRunner를 cls에서 받아옴
 *  - saveOne, saveMany, removeOne, removeMany는 공통 로직으로 여기에 구현해둠
 *  - find는 트랜잭션을 사용하지 않을것 같음
 * */
export abstract class TypeormRepository<T extends BaseEntity>
  extends Repository<T>
  implements BaseRepository<T>, OnModuleInit
{
  clsService?: ClsService;

  onModuleInit() {
    const application = getApplication();

    this.clsService = application.get(ClsService);
  }

  createOne(entity: T): T {
    return this.create(entity);
  }

  createMany(entities: T[]): T[] {
    return this.create(entities);
  }

  /** @description
   *  save는 단건 전용, save 후에 select 쿼리 나가는거 끄려면 reload: false
   * */
  async saveOne(entity: T): Promise<number> {
    const { queryRunner, fromCls } = await this.getQueryRunner();

    const createdEntity = await queryRunner.manager.save(entity);

    if (!fromCls) await queryRunner.release();

    return createdEntity.id;
  }

  /** @description
   *  insert는 bulk 전용
   * */
  async saveMany(entities: T[]): Promise<number[]> {
    if (entities.length === 0) {
      return [];
    }

    const { queryRunner, fromCls } = await this.getQueryRunner();

    const createdEntities = await queryRunner.manager.insert(this.target, entities as QueryDeepPartialEntity<T>[]);

    if (!fromCls) await queryRunner.release();

    return createdEntities.identifiers.map((obj) => obj.id);
  }

  /** @description
   *  save로 update하면 select 쿼리가 여러번 나가는 문제가 있음
   * */
  async updateOne(entity: T): Promise<number> {
    const { queryRunner, fromCls } = await this.getQueryRunner();

    const criteria = { id: entity.id };
    const partialEntity = this.filterNoneRelationProperty(entity);

    await queryRunner.manager.update(this.target, criteria, partialEntity as QueryDeepPartialEntity<T>);

    if (!fromCls) await queryRunner.release();

    return entity.id;
  }

  /** @description
   *  remove는가 단건 전용이지만, 불필요한 select query가 나가서 delete로 구현함
   * */
  async deleteOne(entity: T): Promise<number> {
    const { queryRunner, fromCls } = await this.getQueryRunner();

    const entityId = entity.id;
    await queryRunner.manager.delete(this.target, entityId);

    if (!fromCls) await queryRunner.release();

    return entityId;
  }

  /** @description
   *  delete는 bulk 전용
   * */
  async deleteMany(entities: T[]): Promise<number[]> {
    if (entities.length === 0) {
      return [];
    }

    const { queryRunner, fromCls } = await this.getQueryRunner();

    const entityIds = entities.map((entity) => entity.id);
    await queryRunner.manager.delete(this.target, entityIds);

    if (!fromCls) await queryRunner.release();

    return entityIds;
  }

  async deleteAll(): Promise<void> {
    const { queryRunner, fromCls } = await this.getQueryRunner();

    await queryRunner.manager.delete(this.target, {});

    if (!fromCls) await queryRunner.release();
  }

  async findAll(): Promise<T[]> {
    return this.find();
  }

  async findOneByIdOrNull(id: number): Promise<T | null> {
    return this.createQueryBuilder('entity').where('entity.id = :id', { id }).getOne();
  }

  async findOneById(id: number): Promise<T> {
    const entity = await this.createQueryBuilder('entity').where('entity.id = :id', { id }).getOne();
    if (!entity) {
      throw new NotFoundException(`cannot found ${this.target} by id: ${id}`);
    }

    return entity;
  }

  async findManyByIds(ids: number[]): Promise<T[]> {
    return this.createQueryBuilder('entity').where('entity.id in (:...ids)', { ids }).getMany();
  }

  private async getQueryRunner(): Promise<{ queryRunner: QueryRunner; fromCls: boolean }> {
    const trxQueryRunner = this.clsService?.get('QUERY_RUNNER');
    if (trxQueryRunner) {
      return { queryRunner: trxQueryRunner, fromCls: true };
    }

    const newQueryRunner = this.manager.connection.createQueryRunner();

    return { queryRunner: newQueryRunner, fromCls: false };
  }

  /** @description
   *  relation property을 제외한 primitive, date 컬럼만 필터링
   */
  private filterNoneRelationProperty(entity: T) {
    return Object.fromEntries(
      Object.entries(entity).filter(([key, value]) => {
        if (key === 'id') return false;

        const isPrimitiveColumn = ['string', 'number', 'bigint', 'boolean', 'undefined'].includes(typeof value);

        const isCustomDateColumn = !['createdAt', 'updatedAt', 'deletedAt'].includes(key) && value instanceof Date;

        return isPrimitiveColumn || isCustomDateColumn;
      }),
    );
  }
}
