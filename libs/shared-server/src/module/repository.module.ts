/* eslint-disable @typescript-eslint/no-explicit-any */
import { InternalServerErrorException, Provider } from '@nestjs/common';
import { getDataSourceToken } from '@nestjs/typeorm';
import { BaseRepository, REPOSITORY_DECORATOR } from 'libs/shared-server/src/domain';
import { TypeormRepository } from 'libs/shared-server/src/infrastructure';
import { DataSource } from 'typeorm';

/** @description
 *  repository DI할때 사용할 유틸성 모듈
 *  - @Repository 데코레이터로 등록한 구현체를 넣어줌
 * */
export class RepositoryModule {
  public static register(...repositories: typeof BaseRepository<any>[]) {
    const entities: any[] = [];
    const providers: Provider[] = [];

    for (const repo of repositories) {
      const [entity, repoImpl] = Reflect.getMetadata(REPOSITORY_DECORATOR, repo);

      if (!entity || !(repoImpl.prototype instanceof TypeormRepository)) {
        throw new InternalServerErrorException(`Invalid entity or repository repo: ${repo}`);
      }

      entities.push(entity);

      providers.push({
        inject: [getDataSourceToken()],
        provide: repo,
        useFactory: (dataSource: DataSource) => {
          const baseRepository = dataSource.getRepository(entity);

          return new repoImpl(baseRepository.target, baseRepository.manager, baseRepository.queryRunner);
        },
      });
    }

    return [entities, providers];
  }
}
