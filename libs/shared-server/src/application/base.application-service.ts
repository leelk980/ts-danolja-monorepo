import { getApplication } from '@danolja/shared-server';
import { BadRequestException, OnModuleInit } from '@nestjs/common';
import { ClsService } from 'nestjs-cls';
import { DataSource } from 'typeorm';

/**
 * @description
 * 트랜잭션 매니저
 * */
export abstract class BaseApplicationService implements OnModuleInit {
  dataSource?: DataSource;
  clsService?: ClsService;

  // app.listen() 해야 실행됨
  onModuleInit() {
    const application = getApplication();

    this.dataSource = application.get(DataSource);
    this.clsService = application.get(ClsService);
  }

  getCurrentUserId(): number {
    const userId = this.clsService?.get('userId');
    if (!userId || typeof userId !== 'number') {
      throw new BadRequestException(`invalid userId: ${userId}`);
    }

    return userId;
  }
}

/**
 * @description
 * 트랜잭션 사용할 메소드 명시
 * */
export const Transaction = () => {
  return function (
    _target: any,
    _name: string,
    descriptor: TypedPropertyDescriptor<any>,
  ): TypedPropertyDescriptor<any> {
    const originalMethod = descriptor.value;

    return {
      configurable: true,
      enumerable: false,
      get() {
        return async (...args: any[]) => {
          const that = this as BaseApplicationService;
          if (!that.dataSource || !that.clsService) {
            return originalMethod.call(that, ...args);
          }

          const queryRunner = that.dataSource.createQueryRunner();
          await queryRunner.connect();

          that.clsService.set('QUERY_RUNNER', queryRunner);

          let result: any;
          let error: any;
          try {
            await queryRunner.startTransaction();

            result = await originalMethod.call(that, ...args);

            await queryRunner.commitTransaction();
          } catch (err) {
            error = err;
            await queryRunner.rollbackTransaction();
          } finally {
            await queryRunner.release();
          }

          if (error) throw error;
          return result;
        };
      },
    };
  };
};
