type KeyAble = string | number | symbol;

declare global {
  interface Array<T extends Record<KeyAble, any>> {
    associateBy(key: keyof T): Record<KeyAble, T>;

    groupBy(key: keyof T, values?: KeyAble[]): Record<KeyAble, T[]>;

    sortBy(key: keyof T, order: 'asc' | 'desc'): Array<T>;
  }
}

Array.prototype.associateBy = function (key) {
  // noinspection UnnecessaryLocalVariableJS
  const singleMap = this.reduce((acc, each) => {
    acc[each[key]] = each;
    return acc;
  }, {});

  return singleMap;
};

Array.prototype.groupBy = function (key, values?) {
  const groupMap = this.reduce((acc: Record<KeyAble, any[]>, each) => {
    if (!acc[each[key]]) {
      acc[each[key]] = [each];
    } else {
      acc[each[key]] = [...acc[each[key]], each];
    }

    return acc;
  }, {});

  if (!values) {
    return groupMap;
  }

  for (const id of values) {
    if (!groupMap[id]) groupMap[id] = [];
  }

  return groupMap;
};

Array.prototype.sortBy = function (key, order) {
  if (order === 'asc') {
    return this.sort((a, b) => a[key] - b[key]);
  }

  if (order === 'desc') {
    return this.sort((a, b) => b[key] - a[key]);
  }

  return this;
};

export {};
