export type BaseSocketEvent<T> = {
  name: string;

  payload: T;
};

const asSocketEventRecord = <T extends Record<string, BaseSocketEvent<unknown>>>(obj: T) => obj;

export const SocketEvent = asSocketEventRecord({
  quizRoomUpdated: {
    name: 'quiz-room-updated',
    payload: {} as { id: number; type: 'join' | 'start' | 'refetch' },
  },
});
