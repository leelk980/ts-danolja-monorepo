import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';

export class QuizRoomsIdGetView {
  name!: string;
  status!: 'ready' | 'proceeding' | 'finish';
  accessCode!: string;

  quizRoomUsers!: {
    id: number;
    userId: number;
    nickname: string;
    isOwner: boolean;
    totalScore: number;
  }[];

  quizRoomChats!: {
    id: number;
    quizRoomUserId: number;
    message: string;
    isBot: boolean;
    createdAt: Date;
  }[];

  currentQuiz!: {
    id: number;
    score: number;
    type: 'checkbox' | 'radio' | 'input';
    title: string;
    subTitle: string;
    isManaged: boolean;

    quizCategories: {
      id: number;
      name: string;
      description: string;
    }[];
    quizOptions: {
      id: number;
      title: string;
      subTitle: string;
    }[];
  } | null;
}

export class QuizRoomsIdPatchBody {
  @IsOptional()
  @IsString()
  name?: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => QuizRoomQuizBody)
  quizRoomQuizzes?: QuizRoomQuizBody[];
}

class QuizRoomQuizBody {
  @IsNumber()
  id!: number;

  @IsNumber()
  score!: number;
}
