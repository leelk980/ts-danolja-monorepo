import { IsString } from 'class-validator';

export class QuizRoomsPostBody {
  @IsString()
  quizRoomName!: string;

  @IsString()
  nickname!: string;
}

export class QuizRoomsPutBody {
  @IsString()
  nickname!: string;

  @IsString()
  accessCode!: string;
}
