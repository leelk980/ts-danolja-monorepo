import { IsString } from 'class-validator';

export class QuizRoomsIdChatsPostBody {
  @IsString()
  message!: string;
}
