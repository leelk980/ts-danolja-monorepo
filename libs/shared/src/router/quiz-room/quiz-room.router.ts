import { asRouterRecord } from 'libs/shared/src/router/base.router';
import { QuizRoomsIdChatsPostBody } from 'libs/shared/src/router/quiz-room/quiz-rooms-id-chats.dto';
import { QuizRoomsIdGetView, QuizRoomsIdPatchBody } from 'libs/shared/src/router/quiz-room/quiz-rooms-id.dto';
import { QuizRoomsPostBody, QuizRoomsPutBody } from 'libs/shared/src/router/quiz-room/quiz-rooms.dto';

export const QuizRoomRouter = asRouterRecord({
  quizRooms: {
    /** @description 퀴즈룸 생성하기 */
    post: {
      uri: '/quiz-rooms',
      method: 'post',
      auth: 'user',
      body: new QuizRoomsPostBody(),
      view: 1,
    },

    /** @description 퀴즈룸에 참여하기 */
    put: {
      uri: '/quiz-rooms',
      method: 'put',
      auth: 'user',
      body: new QuizRoomsPutBody(),
      view: 1,
    },
  },

  quizRoomsId: {
    /** @description id로 퀴즈룸 조회하기, 참여중인 멤버만 가능 */
    get: {
      uri: '/quiz-rooms/:id',
      method: 'get',
      auth: 'user',
      queryString: undefined,
      view: new QuizRoomsIdGetView(),
    },

    /** @description 퀴즈룸 이름, 퀴즈 수정하기 => 퀴즈 시작하기 */
    patch: {
      uri: '/quiz-rooms/:id',
      method: 'patch',
      auth: 'user',
      body: new QuizRoomsIdPatchBody(),
      view: 1,
    },
  },

  quizRoomsIdChats: {
    /** @description 채팅 메시지 보내기 */
    post: {
      uri: '/quiz-rooms/:id/chats',
      method: 'post',
      auth: 'user',
      body: new QuizRoomsIdChatsPostBody(),
      view: 1,
    },
  },
});
