type Auth = 'public' | 'user';

type GetApi<T, U> = {
  uri: string;
  method: 'get';
  auth: Auth;
  queryString: T;
  view: U;
};

type PostApi<T, U> = {
  uri: string;
  method: 'post';
  auth: Auth;
  body: T;
  view: U;
};

type PutApi<T, U> = {
  uri: string;
  method: 'put';
  auth: Auth;
  body: T;
  view: U;
};

type PatchApi<T, U> = {
  uri: string;
  method: 'patch';
  auth: Auth;
  body: T;
  view: U;
};

type DeleteApi<T, U> = {
  uri: string;
  method: 'delete';
  auth: Auth;
  body: T;
  view: U;
};

export type ServerApi =
  | GetApi<unknown, unknown>
  | PostApi<unknown, unknown>
  | PutApi<unknown, unknown>
  | PatchApi<unknown, unknown>
  | DeleteApi<unknown, unknown>;

export const asRouterRecord = <
  T extends Record<
    string,
    {
      get?: GetApi<unknown, unknown>;
      post?: PostApi<unknown, unknown>;
      put?: PutApi<unknown, unknown>;
      patch?: PatchApi<unknown, unknown>;
      delete?: DeleteApi<unknown, unknown>;
    }
  >,
>(
  router: T,
) => router;
