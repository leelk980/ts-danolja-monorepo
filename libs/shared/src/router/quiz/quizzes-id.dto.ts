export class QuizzesIdGetView {
  id!: number;
  type!: 'checkbox' | 'radio' | 'input';
  title!: string;
  subTitle!: string;
  isManaged!: boolean;

  quizCategories!: {
    id: number;
    name: string;
    description: string;
  }[];

  quizOptions!: {
    id: number;
    title: string;
    subTitle: string;
    isCorrect: boolean | null;
  }[];
}
