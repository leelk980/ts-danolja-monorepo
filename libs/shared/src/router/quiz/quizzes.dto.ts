import { IsNumber } from 'class-validator';

export class QuizzesGetQuery {
  @IsNumber()
  categoryId!: number;
}

export class QuizzesGetView {
  quizzes!: {
    id: number;
    type: 'checkbox' | 'radio' | 'input';
    title: string;
    subTitle: string;
    isManaged: boolean;
  }[];

  pagination!: {
    count: number;
  };
}
