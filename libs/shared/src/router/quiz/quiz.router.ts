import { asRouterRecord } from 'libs/shared/src/router/base.router';
import { QuizzesIdGetView } from 'libs/shared/src/router/quiz/quizzes-id.dto';
import { QuizzesGetQuery, QuizzesGetView } from 'libs/shared/src/router/quiz/quizzes.dto';

export const QuizRouter = asRouterRecord({
  quizzes: {
    get: {
      /** @description 퀴즈 목록 가져오기 */
      uri: '/quizzes',
      method: 'get',
      auth: 'user',
      queryString: new QuizzesGetQuery(),
      view: new QuizzesGetView(),
    },
  },

  quizzesId: {
    get: {
      /** @description 퀴즈 상세정보 가져오기 */
      uri: '/quizzes/:id',
      method: 'get',
      auth: 'user',
      queryString: undefined,
      view: new QuizzesIdGetView(),
    },
  },
});
