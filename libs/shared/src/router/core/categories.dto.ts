export class CategoriesGetView {
  categories!: {
    id: number;
    name: string;
    description: string;
  }[];

  pagination!: {
    count: number;
  };
}
