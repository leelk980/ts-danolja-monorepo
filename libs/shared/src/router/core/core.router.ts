import { asRouterRecord } from 'libs/shared/src/router/base.router';
import { CategoriesGetView } from 'libs/shared/src/router/core/categories.dto';

export const CoreRouter = asRouterRecord({
  categories: {
    get: {
      uri: '/categories',
      method: 'get',
      auth: 'user',
      queryString: undefined,
      view: new CategoriesGetView(),
    },
  },
});
