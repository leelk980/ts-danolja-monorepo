import 'reflect-metadata';
import { CoreRouter } from './core/core.router';
import { QuizRoomRouter } from './quiz-room/quiz-room.router';
import { QuizRouter } from './quiz/quiz.router';
import { UserRouter } from './user/user.router';

export const ApiRouter = {
  ...CoreRouter,
  ...QuizRoomRouter,
  ...QuizRouter,
  ...UserRouter,
};

export * from './base.router';

export * from './core/categories.dto';

export * from './quiz/quizzes-id.dto';
export * from './quiz/quizzes.dto';

export * from './quiz-room/quiz-rooms.dto';
export * from './quiz-room/quiz-rooms-id.dto';
export * from './quiz-room/quiz-rooms-id-chats.dto';

export * from './user/sign-in.dto';
export * from './user/user-me.dto';
