export class UserMeGetView {
  id!: number;
  email!: string;
  name!: string;
}
