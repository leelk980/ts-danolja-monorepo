import { IsEmail } from 'class-validator';

export class SignInPostBody {
  @IsEmail()
  email!: string;
}

export class SignInPostView {
  accessToken!: string;
  refreshToken!: string;
}
