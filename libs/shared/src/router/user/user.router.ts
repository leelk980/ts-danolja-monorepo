import { asRouterRecord } from 'libs/shared/src/router/base.router';
import { SignInPostBody, SignInPostView } from 'libs/shared/src/router/user/sign-in.dto';
import { UserMeGetView } from 'libs/shared/src/router/user/user-me.dto';

export const UserRouter = asRouterRecord({
  ssoKakao: {
    get: {
      uri: '/sso/kakao',
      method: 'get',
      auth: 'public',
      queryString: undefined,
      view: -1,
    },
  },

  ssoKakaoCallback: {
    get: {
      uri: '/sso/kakao/callback',
      method: 'get',
      auth: 'public',
      queryString: undefined,
      view: -1,
    },
  },

  signIn: {
    post: {
      uri: '/sign-in',
      method: 'post',
      auth: 'public',
      body: new SignInPostBody(),
      view: new SignInPostView(),
    },
  },

  userMe: {
    get: {
      uri: '/user-me',
      method: 'get',
      auth: 'user',
      queryString: undefined,
      view: new UserMeGetView(),
    },
  },
});
