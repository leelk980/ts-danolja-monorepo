# NX 기반의 다놀자 모노레포

- 신서유기, 놀라운 토요일 같은 예능에서 하는 퀴즈를 푸는 서비스 (오프라인 + 온라인)
- 서비스 플로우
  - 회원가입
  - 방생성 or 방참가
  - 방장이 퀴즈를 설정하고 게임 시작
  - 정답 제출

- 필요한 기능
  - todo

## 디렉토리 구조

### apps

개별적인 어플리케이션 프로젝트

- server: api server
- server-admin: admin api server (추가 예정)
- web: next.js web front
- web-admin: admin web front (추가예정)

### libs

apps의 프로젝트가 사용하는 라이브러리

- shared
  - frontend와 backend가 공유하는 라이브러리
  - ex) api docs, utilities, 확장함수 등
- shared-server
  - server 어플리케이션끼리 공유하는 라이브러리
  - ex) domain entity, base.*.ts

### server 설명

- nest.js + typeorm + mysql
- 1개의 모듈 + 레이어드 아키텍쳐
  - app.module.ts
  - interface
  - application
  - domain
  - infrastructure

### web

- next.js + react-query + chakraui
- api server와 통신할때는 custom api hook을 사용
  - useApiInvalidation
  - useApiMutation
  - useApiQuery
